module.exports = {
  root: true,
  env: {
    'es6': true,
    node: true,
    jquery: true
  },
  'extends': [
    '@vue/standard',
    'plugin:vue/base',
    'plugin:vue/essential',
    // 'plugin:vue/strongly-recommended',
    'plugin:vue/recommended'
  ],
  rules: {
    'properties': 'never',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'jsx-quotes': ['error', 'prefer-single'],
    'vue/require-prop-types': 'off',
    'vue/require-default-prop': 'off',
    'vue/attribute-hyphenation': 'off',
    'vue/prop-name-casing': 'off',
    'vue/camel-case': 'off'
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
