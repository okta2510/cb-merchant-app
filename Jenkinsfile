pipeline {
    
    agent {
        label 'master'
    }

    environment {
        SERVICE_NAME = "cb-webapp-spa"
        AWS_ACCOUNT_ID = "150368139372"
        AWS_REGION = "ap-southeast-1"
        AWS_ECR_URL = "${env.AWS_ACCOUNT_ID}.dkr.ecr.${env.AWS_REGION}.amazonaws.com/${env.SERVICE_NAME}"
        AWS_ECR_CREDS = "ecr:ap-southeast-1:ecr-credential"
        SOURCE_REPO_URL = "git@code.xwhyc.com:cashbac/${env.SERVICE_NAME}.git"
        CONFIG_REPO_URL = "git@code.xwhyc.com:cashbac/cb-config.git"
        HELM_CHARTS_REPO_URL = "git@code.xwhyc.com:devops/cb-eks-yaml"
        BUILD_VERSION = sh(returnStdout: true, script: "git tag -l --sort=creatordate | tail -1").trim()
    }


    parameters {
        booleanParam(
            name: 'hotfix',
            description: 'Whether it is a hotfix deployment or not.',
            defaultValue: false
        )
    }
    
    stages {

        stage ('Check Deployment Time') {
            steps {
                script {
                    if (!params.hotfix) {
                        sh 'python3.6 /home/centos/checktime/check_time.py'
                    }

                    if (params.hotfix) {
                        echo "Granted for hotfix deployment..."
                    }
                }
            }
        }
        
        stage ('Checkout') {
            steps {
                // Send Slack message.
                slackSend (
                    channel: '#cb_prod_notification',
                    color: "warning",
                    message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Started (<${env.BUILD_URL}|Open>)"
                )

                // Cleanup workspace.
                cleanWs()

                // Clone configuration reposository into "ansible" directory.
                checkout scm: [
                    $class: 'GitSCM',
                    userRemoteConfigs: [
                        [
                            url: "${env.CONFIG_REPO_URL}",
                            credentialsId: "${env.gitCredential}"
                        ]
                    ],
                    branches: [
                        [
                            name: '*/master'
                        ]
                    ],
                    extensions: [
                        [
                            $class: 'RelativeTargetDirectory',
                            relativeTargetDir: 'ansible'
                        ]
                    ]
                ],
                poll: false

                // Generate configuration file(s).
                sh """
                    cd ${env.WORKSPACE}/ansible
                    ansible-playbook cb-config.yml --extra-vars \"env_type=prod service_name=${env.SERVICE_NAME}\"
                """

                // Clone source code repository into "dist" directory.
                checkout scm: [
                    $class: 'GitSCM',
                    userRemoteConfigs: [
                        [
                            url: "${env.SOURCE_REPO_URL}",
                            credentialsId: "${env.gitCredential}"
                        ]
                    ],
                    branches: [
                        [
                            name: "${env.BUILD_VERSION}"
                        ]
                    ],
                    extensions: [
                        [
                            $class: 'RelativeTargetDirectory',
                            relativeTargetDir: 'dist'
                        ]
                    ]
                ],
                poll: false

                // Move configuration file(s).
                // sh "mv -f ${env.WORKSPACE}/ansible/files/cbconfig/${env.SERVICE_NAME}/app.conf ${env.WORKSPACE}/dist/conf/app.conf"
            }
        }

        stage ('Dockerfile Linter') {
            steps {
                script {
                    sh "docker run --rm -v ${env.WORKSPACE}/dist:/root/ -i projectatomic/dockerfile-lint dockerfile_lint --dockerfile Dockerfile"
                }
            }
        }

        stage ('Bugs and Security Checking') {
            steps {
                sh """
                    cd ${env.WORKSPACE}/dist
                    source /opt/python-virtualenv/semgrep/bin/activate
                    semgrep --config p/javascript --config p/nodejsscan --config p/owasp-top-ten . --junit-xml --output cb-webapp-spa.xml
                    junit2html cb-webapp-spa.xml semgrep-reports.html
                    deactivate
                    mkdir -p ${env.JENKINS_HOME}/userContent/cb-webapp-spa
                    cd ${env.JENKINS_HOME}/userContent/cb-webapp-spa && rm -vrf ./*
                    cd ${env.WORKSPACE}/dist && cp -vf semgrep-reports.html ${env.JENKINS_HOME}/userContent/cb-webapp-spa/
                """
                script {
                    env.SEMGREP_FINDINGS = sh (script: "grep 'Failures' semgrep-reports.html | grep -o '<td>.*</td>' |  awk -F '</?td>' '{print \$2}'", returnStdout: true)
                }
                slackSend (channel: '#cb_prod_notification', color: "warning", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} - Issue(s) found: ${env.SEMGREP_FINDINGS} - <${env.JENKINS_URL}/userContent/cb-webapp-spa/semgrep-reports.html|See the reports here>")
            }
        }

        stage ('Build Package') {
            steps {
                script {
                sh """
                    cd ${env.WORKSPACE}/dist
                    unlink /usr/bin/npm
                    ln -s /root/.nvm/versions/node/v12.3.1/bin/npm /usr/bin/npm
                    unlink /usr/bin/node
                    ln -s /root/.nvm/versions/node/v12.3.1/bin/node /usr/bin/node
                    rm -rf node_modules
                    npm install
                    npm run build
                """
                }
            }
        }

        stage ('Build Image') {
            steps {
                script {
                    dockerImage = docker.build("${env.AWS_ECR_URL}:${env.BUILD_VERSION}", "${env.WORKSPACE}/dist")
                }
            }
        }

        stage ('Push Image') {
            steps {
                script {
                    docker.withRegistry("https://${env.AWS_ECR_URL}",, "${env.AWS_ECR_CREDS}") {
                        dockerImage.push("${env.BUILD_VERSION}")
                        dockerImage.push("latest")
                    }
                }
            }
        }

        stage ('Remove Local Image ') {
            steps {
                sh ("docker rmi -f ${env.AWS_ECR_URL}:${env.BUILD_VERSION}")
                sh ("docker rmi ${env.AWS_ECR_URL}:latest")
            }
        }

        stage ('Deploy') {
            steps {
                // Cleanup workspace.
                cleanWs()

                // Clone Helm charts repository.
                checkout scm: [
                    $class: 'GitSCM',
                    userRemoteConfigs: [
                        [
                            url: "${env.HELM_CHARTS_REPO_URL}",
                            credentialsId: "${env.gitCredential}"
                        ]
                    ],
                    branches: [
                        [
                            name: '*/master'
                        ]
                    ]
                ],
                poll: false

                // Deploy Helm chart.
                sh """
                    cd ${env.WORKSPACE}/eks-manifests/webapp-spa
                    ansible localhost -m lineinfile -a "path=Chart.yaml regexp='^appVersion:' line='appVersion: \"${env.BUILD_VERSION}\"'"
                    /usr/local/bin/helm upgrade --atomic --timeout 90s --install --set image.tag=${env.BUILD_VERSION} ${env.SERVICE_NAME} . --namespace ${env.NAMESPCACE_EKS_PROD}
                """
            }
        }
    }

    post {

        success {
            slackSend (
                channel: '#cb_prod_notification',
                color: "good",
                message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Success (<${env.BUILD_URL}|Open>) "
            )
        }

        failure {
            slackSend (
                channel: '#cb_prod_notification',
                color: "danger",
                message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Failed (<${env.BUILD_URL}|Open>) "
            )
        }
    }
}
