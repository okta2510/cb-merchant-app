module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  crossorigin: 'use-credentials',
  devServer: {
    proxy: {
      '^/1.0': {
        target: 'http://172.32.2.108:20400',
        ws: true,
        changeOrigin: true
        // pathRewrite: { '^/app': '' }
      },
      '^/oauth': {
        target: 'http://172.32.2.108:9100',
        ws: true,
        changeOrigin: true
      }
    },
    overlay: {
      warnings: true,
      errors: true
    },
    port: 8200
  }
}
