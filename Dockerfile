FROM node:12.3.1-alpine

LABEL Name="cb-webapp-spa" \
       Version="1.0"

WORKDIR /root/cb-webapp-spa

COPY package*.json ./

# To handle 'not get uid/gid'
RUN npm config set unsafe-perm true

RUN npm install -g angular-http-server

COPY . .

RUN rm -rf /root/cb-webapp-spa/node_modules

EXPOSE 8080

CMD [ "angular-http-server", "--path", "dist/" ]