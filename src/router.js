import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/dashboard',
      name: 'home',
      component: Home
    },
    {
      path: '/',
      name: 'root',
      redirect: '/auth/login'
    },
    {
      path: '/auth/login',
      name: 'login',
      props: true,
      component: () => import('./views/Login.vue')
    },
    {
      path: '/auth/logout',
      name: 'Logout',
      props: true,
      component: () => import('./views/Logout.vue')
    },
    {
      path: '/transactions',
      name: 'TransactionBase',
      props: true,
      component: () => import('./views/transactions/TransactionBase.vue'),
      redirect: '/transactions/list',
      meta: {
        breadcrumb: 'Transactions',
        redirect: '/transactions/list'
      },
      children: [
        {
          path: '/transactions/list',
          name: 'TransactionList',
          props: true,
          meta: {
            primary: true,
            second: 'List',
            breadcrumb: 'List',
            redirect: ''
          },
          component: () => import('./views/transactions/TransactionList.vue')
        },
        {
          path: '/transactions/detail/:pubId',
          name: 'TransactionsDetail',
          props: true,
          meta: {
            second: localStorage.getItem('infoParam'),
            breadcrumb: 'Detail',
            redirect: ''
          },
          component: () => import('./views/transactions/TransactionDetail.vue')
        }
      ]
    },
    {
      path: '/deals',
      name: 'deals',
      component: () => import('./views/deals/DealBase.vue'),
      redirect: '/deals/list',
      meta: {
        breadcrumb: 'Deals',
        redirect: '/deals/list'
      },
      children: [
        {
          path: '/deals/detail/:id',
          name: 'deals_view',
          props: true,
          meta: {
            // second: '',
            second: localStorage.getItem('infoParam'),
            breadcrumb: 'Detail',
            redirect: ''
          },
          component: () => import('./views/deals/DealView.vue')
        },
        {
          path: '/deals/list',
          name: 'deals_list',
          meta: {
            primary: true,
            second: 'List',
            breadcrumb: 'List',
            redirect: ''
          },
          props: (route) => ({ query: route.query.q }),
          component: () => import('./views/deals/DealList.vue')
        },
        {
          path: '/deals/create',
          name: 'deals_create',
          meta: {
            primary: true,
            breadcrumb: 'Create',
            second: 'Create',
            redirect: ''
          },
          component: () => import('./views/deals/DealCreate.vue')
        },
        {
          path: '/deals/edit/:id',
          name: 'deals_edit',
          props: true,
          meta: {
            second: 'Edit',
            breadcrumb: 'Edit',
            redirect: ''
          },
          component: () => import('./views/deals/DealEdit.vue')
        }
      ]
    },
    {
      path: '/withdrawal',
      name: 'WithdrawalBase',
      props: true,
      component: () => import('./views/withdrawals/WithdrawalBase.vue'),
      redirect: '/withdrawal/regular',
      meta: {
        breadcrumb: 'Withdrawal',
        redirect: '/withdrawal/regular'
      },
      children: [
        {
          path: '/withdrawal/regular/:pubId',
          name: 'WithdrawalRegularDetail',
          meta: {
            primary: true,
            breadcrumb: 'Regular',
            second: 'Detail',
            redirect: '/withdrawal/regular?pagination=1'
          },
          component: () => import('./views/withdrawals/WithdrawalPay.vue')
        },
        {
          path: '/withdrawal/regular',
          name: 'WithdrawalRegular',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Regular',
            second: '',
            redirect: ''
          },
          component: () => import('./views/withdrawals/WithdrawalRegular.vue')
        },
        {
          path: '/withdrawal/deals/:pubId',
          name: 'WithdrawalDealDetail',
          meta: {
            primary: true,
            breadcrumb: 'Deals',
            second: 'Detail',
            redirect: '/withdrawal/deals?pagination=1'
          },
          component: () => import('./views/withdrawals/WithdrawalPayDeals.vue')
        },
        {
          path: '/withdrawal/deals',
          name: 'WithdrawalDeals',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Deals',
            second: ' List',
            redirect: ''
          },
          component: () => import('./views/withdrawals/WithdrawalDeals.vue')
        }
      ]
    },
    {
      path: '/settlements',
      name: 'SettlementsBase',
      props: true,
      component: () => import('./views/settlements/SettlementsBase.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Settlements',
        second: 'Settlements',
        redirect: '/'
      },
      children: [
        {
          path: '/settlements/regular/list',
          name: 'SettlementsRegular',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Regular',
            second: ' List',
            redirect: '/'
          },
          component: () => import('./views/settlements/SettlementsRegular.vue')
        },
        {
          path: '/settlements/deals/list',
          name: 'SettlementsDeals',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Deals',
            second: ' List',
            redirect: '/'
          },
          component: () => import('./views/settlements/SettlementsDeals.vue')
        },
        {
          path: '/settlement/deals/create',
          name: 'TransactionDeals',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Deals',
            second: 'Create Settlements',
            redirect: '/settlements/deals/list'
          },
          component: () => import('./views/settlements/TransactionDeals.vue')
        },
        {
          path: '/settlement/regular/create',
          name: 'TransactionRegular',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Regular',
            second: 'Create Settlements',
            redirect: '/settlements/regular/list'
          },
          component: () => import('./views/settlements/TransactionRegular.vue')
        },
        {
          path: '/settlements/deals/:pubId/failed',
          name: 'SettlementFailedDeals',
          // redirect: '/',
          props: true,
          meta: {
            breadcrumb: 'Settlement Failed',
            redirect: '/settlements/deals/:pubId/failed'
          },
          component: () => import('./views/settlements/SettlementFailedDeals.vue')
        },
        {
          path: '/settlements/regular/:pubId/failed',
          name: 'SettlementFailed',
          // redirect: '/',
          props: true,
          meta: {
            breadcrumb: 'Settlement Failed',
            redirect: '/settlements/regular/:pubId/failed'
          },
          component: () => import('./views/settlements/SettlementFailed.vue')
        },
        {
          path: '/settlements/regular/:pubId',
          name: 'SettlementSubmit',
          props: true,
          meta: {
            second: 'Detail - ',
            breadcrumb: 'Reguler',
            redirect: '/settlements/regular/list'
          },
          component: () => import('./views/settlements/SettlementSubmit.vue')
        },
        {
          path: '/settlements/deals/:pubId',
          name: 'SettlementSubmitDeal',
          props: true,
          meta: {
            second: 'Detail - ',
            breadcrumb: 'Deals',
            redirect: '/settlements/deals/list'
          },
          component: () => import('./views/settlements/SettlementSubmitDeals.vue')
        }
      ]
    },
    {
      path: '/users',
      name: 'UsersBase',
      props: true,
      component: () => import('./views/users/UsersBase.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Dashboard Users',
        second: 'Dashboard Users',
        redirect: '/users/list'
      },
      children: [
        {
          path: '/users/list',
          name: 'UsersList',
          props: true,
          meta: {
            primary: false,
            breadcrumb: 'List',
            second: '',
            redirect: ''
          },
          component: () => import('./views/users/UsersList.vue')
        }
      ]
    },
    {
      path: '/campaigns',
      name: 'CampaignsBase',
      props: true,
      component: () => import('./views/campaigns/CampaignsBase.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Campaigns',
        second: 'Campaigns',
        redirect: '/campaigns/list'
      },
      children: [
        {
          path: '/campaigns/list',
          name: 'CampaignsList',
          props: true,
          meta: {
            primary: false,
            breadcrumb: 'List',
            second: '',
            redirect: ''
          },
          component: () => import('./views/campaigns/CampaignsList.vue')
        },
        {
          path: '/campaigns/create',
          name: 'CampaignsCreate',
          props: true,
          meta: {
            second: '',
            breadcrumb: 'Create',
            redirect: ''
          },
          component: () => import('./views/campaigns/CampaignsCreate.vue')
        },
        {
          path: '/campaigns/:pubId',
          name: 'CampaignsDetail',
          props: true,
          meta: {
            second: '',
            breadcrumb: '',
            redirect: ''
          },
          component: () => import('./views/campaigns/CampaignsDetail.vue')
        },
        {
          path: '/campaigns/:pubId/edit',
          name: 'CampaignsEdit',
          props: true,
          meta: {
            second: '',
            breadcrumb: 'Edit',
            redirect: ''
          },
          component: () => import('./views/campaigns/CampaignsEdit.vue')
        }
      ]
    },
    {
      path: '/business-setup',
      name: 'BusinessSetupBase',
      props: true,
      component: () => import('./views/BusinessSetup/base.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Business Setup',
        second: '',
        redirect: '/business-setup/merchants/list'
      },
      children: [
        {
          path: '/business-setup/merchants/list',
          name: 'MerchantList',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Merchants',
            second: 'List',
            redirect: '/business-setup/merchants/list'
          },
          component: () => import('./views/BusinessSetup/MerchantList.vue')
        },
        {
          path: '/business-setup/merchant/:pubid/detail',
          name: 'MerchantDetail',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Merchants',
            second: 'Detail',
            redirect: '/business-setup/merchants/list'
          },
          component: () => import('./views/BusinessSetup/MerchantDetail.vue')
        },
        {
          path: '/business-setup/merchant/:pubid/settlements',
          name: 'MerchantSettlements',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Merchants',
            second: ' Settlements',
            redirect: '/business-setup/merchants/list'
          },
          component: () => import('./views/BusinessSetup/SettlementList.vue')
        },
        {
          path: '/business-setup/merchants/create',
          name: 'MerchantCreate',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Merchants',
            second: 'Create',
            redirect: '/business-setup/merchants/list'
          },
          component: () => import('./views/BusinessSetup/MerchantCreate.vue')
        },
        {
          path: '/business-setup/merchant/:id/edit',
          name: 'MerchantEdit',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Merchants',
            second: 'Edit',
            redirect: '/business-setup/merchants/list'
          },
          component: () => import('./views/BusinessSetup/MerchantEdit.vue')
        },
        {
          path: '/business-setup/outlets/list',
          name: 'OutletList',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Outlet',
            second: 'List',
            redirect: '/business-setup/outlets/list'
          },
          component: () => import('./views/BusinessSetup/OutletList.vue')
        },
        {
          path: '/business-setup/outlet/create',
          name: 'OutletCreate',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Outlet',
            second: ' Create',
            redirect: '/business-setup/outlets/list'
          },
          component: () => import('./views/BusinessSetup/OutletCreate.vue')
        },
        {
          path: '/business-setup/outlet/:pubId/detail',
          name: 'OutletDetail',
          props: true,
          meta: {
            breadcrumb: 'Outlet',
            segment: true,
            second: 'Detail',
            redirect: '/business-setup/outlets/list'
          },
          component: () => import('./views/BusinessSetup/OutletDetail.vue')
        },
        {
          path: '/business-setup/outlet/:pubId/edit',
          name: 'OutletEdit',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Outlet',
            second: ' Edit',
            redirect: '/business-setup/outlets/list'
          },
          component: () => import('./views/BusinessSetup/OutletEdit.vue')
        },
        {
          path: '/business-setup/outlet/:pubId/payment-interface',
          name: 'OutletPaymentInterface',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Outlet',
            second: 'Payment Interface',
            redirect: '/business-setup/outlets/'
          },
          component: () => import('./views/paymentInterface/paymentBase.vue')
        },
        {
          path: '/business-setup/outlet/:pubId/photos',
          name: 'OutletPhotos',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'Outlet',
            second: 'Photo',
            redirect: '/business-setup/outlets/list/'
          },
          component: () => import('./views/outletPhotos/OutletPhotosBase.vue')
        },
        {
          path: '/business-setup/outlets/import',
          name: 'OutletsImport',
          props: true,
          meta: {
            // breadcrumb: 'Outlet',
            primary: true,
            breadcrumb: 'Outlet',
            second: 'Import',
            redirect: '/business-setup/outlets/list/'
            // redirect: '/business-setup/outlets/import'
          },
          component: () => import('./views/BusinessSetup/OutletImport.vue')
        }
      ]
    },
    {
      name: 'CMSBase',
      path: '/content-settings',
      props: true,
      component: () => import('./views/cms/CMSBase.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Content Settings',
        second: '',
        redirect: '/'
      },
      children: [
        {
          path: '/content-settings/locations/list',
          name: 'CMSLocationList',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Locations',
            second: ' List',
            redirect: '/content-settings/locations'
          },
          component: () => import('./views/cms/location/CMSLocations.vue')
        },
        {
          path: '/content-settings/location/create',
          name: 'CMSLocationCreate',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Locations',
            second: ' Create',
            redirect: '/content-settings/locations'
          },
          component: () => import('./views/cms/location/CMSLocationCreateEdit.vue')
        },
        {
          path: '/content-settings/outlets/list',
          name: 'CMSOutletList',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Outlets',
            second: 'List',
            redirect: '/content-settings/outlets/list'
          },
          component: () => import('./views/cms/outlet/CMSOutlets.vue')
        },
        {
          path: '/content-settings/outlet/create',
          name: 'CMSOutletCreate',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Outlets',
            second: ' Create',
            redirect: '/content-settings/outlets/list'
          },
          component: () => import('./views/cms/outlet/CMSOutletCreateEdit.vue')
        }
      ]
    },
    {
      name: 'RatingBase',
      path: '/ratingreview',
      props: true,
      component: () => import('./views/RatingReview/RatingBase.vue'),
      redirect: '/',
      meta: {
        breadcrumb: 'Rating & Review',
        second: '',
        redirect: '/ratingreview/list'
      },
      children: [
        {
          path: '/ratingreview/list',
          name: 'RatingList',
          props: true,
          meta: {
            segment: true,
            breadcrumb: 'List',
            second: '',
            redirect: ''
          },
          component: () => import('./views/RatingReview/RatingList.vue')
        },
        {
          path: '/ratingreview/:pubId/detail',
          name: 'RatingEdit',
          props: true,
          meta: {
            segment: false,
            primary: true,
            breadcrumb: 'Detail',
            second: '',
            redirect: ''
          },
          component: () => import('./views/RatingReview/RatingDetail.vue')
        }
      ]
    },
    {
      path: '/promo',
      name: 'PromotionsBase',
      props: true,
      component: () => import('./views/promotions/PromotionBase.vue'),
      redirect: '/promo/list',
      meta: {
        breadcrumb: 'Promotion',
        redirect: '/promo/list'
      },
      children: [
        {
          path: '/promo/list',
          name: 'PromotionList',
          props: true,
          meta: {
            primary: true,
            second: 'List',
            breadcrumb: 'List',
            redirect: ''
          },
          component: () => import('./views/promotions/PromotionList.vue')
        },
        {
          path: '/promo/detail/:pubId',
          name: 'PromotionDetail',
          props: true,
          meta: {
            second: localStorage.getItem('infoParam'),
            breadcrumb: 'Detail',
            redirect: ''
          },
          component: () => import('./views/promotions/PromotionDetail.vue')
        },
        {
          path: '/promo/create',
          name: 'PromotionCreate',
          props: true,
          meta: {
            primary: true,
            breadcrumb: 'Create',
            redirect: ''
          },
          component: () => import('./views/promotions/PromotionCreate.vue')
        },
        {
          path: '/promo/:id/edit',
          name: 'PromotionEdit',
          props: true,
          meta: {
            second: localStorage.getItem('infoParam'),
            breadcrumb: 'Edit',
            redirect: ''
          },
          component: () => import('./views/promotions/PromotionEdit.vue')
        }
      ]
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/404',
      name: '404',
      component: () => import('./views/NotFound.vue')
    },
    {
      path: '/uikit',
      name: 'uikit',
      component: () => import('./views/CoreUi.vue'),
      redirect: '/uikit/dashboard',
      children: [
        {
          path: '/uikit/dashboard',
          name: 'uikit_dashboard',
          props: true,
          component: () => import('./views/CoreUiDashboard.vue')
        },
        {
          path: '/uikit/component',
          name: 'uikit_component',
          props: true,
          component: () => import('./views/CoreUiComponent.vue')
        }
      ]
    },
    {
      path: '*',
      name: 'all',
      redirect: '/404'
    }
  ]
})
