import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage)

export default {
  name: 'authMixin',
  beforeCreate: function () {
    let auth = this.$localStorage.get('auth_info')
    if (auth === null) {
      this.$router.push('/auth/login')
    }
  },
  methods: {
    CheckExpired: function () {
      let auth = localStorage.getItem('auth_info')
      auth = JSON.parse(auth)
      if (auth) {
        let now = new Date()
        auth.expires_time = new Date(auth.expires_time)

        if (auth.expires_time < now) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    GetAuth: function () {
      return localStorage.getItem('auth_info')
    },
    ParseAuth: function () {
      return JSON.parse(this.GetAuth())
    },
    GoLogout: function () {
      this.$router.push('/auth/logout', () => {
        this.flash(`Authentication Expired or not valid`, 'error', {
          timeout: 5000
        })
      })
    },
    GetHeader: function (action) {
      return {
        'token': this.ParseAuth().token,
        'user': this.$store.getters['profile/getAuth'].user,
        'email': this.$store.getters['profile/getAuth'].user,
        'page': this.$route.fullPath,
        'menu': this.$route.name,
        'action': action,
        'message': ''
      }
    }
  }
}
