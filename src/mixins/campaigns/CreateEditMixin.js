import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
import * as dummy from '@/utils/dummy'
Vue.use(Vuency)

let CampaignsMixins = {
  name: 'CampaignsMixins',
  data () {
    return {
      campaignAssets: null,
      campaignsDetails: null,
      campaignsDetailsStatus: '',
      campaignAssetsListType: dummy.campaignAssetsListType,
      campaignAssetsListStatus: dummy.campaignAssetsListStatus,
      campaignAssetsListMerchant: [],
      listStatusCampaign: dummy.statusCampaign,
      allSuccess: false,
      imagesUrlS3: null
    }
  },
  mixins: [authMixin],
  computed: {
    API_GET_MERCHANT: function () {
      return `merchant/status?status=ACTIVE`
    },
    API_GET_DETAIL: function () {
      return `cms/campaigns/find/id?campaignId=${this.pubId || null}`
    },
    API_GET_ASSETS: function () {
      return `cms/campaigns_assets/find/campaignid?campaignId=${this.pubId || null}`
    },
    API_CREATE_DETAIL: function () {
      return `cms/campaigns/create`
    },
    API_EDIT_DETAIL: function () {
      return `cms/campaigns/update`
    },
    API_EDIT_ASSET: function () {
      return `cms/campaigns_assets/update`
    },
    API_UPLOAD_IMAGE: function () {
      return `cms/upload?target=CASHBAC_MERCHANT`
    },
    API_CREATE_TYPE: function () {
      return `cms/campaigns_assets/create`
    }
  },
  tasks (t, { timeout }) {
    return {
      getAssetsListMerchant: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        if (this.GetAuth()) {
          let self = this
          yield self.$http.get(`${endpoint + resource}`, {
            headers: self.GetHeader('GET LIST MERCHANT')
          }).then(response => {
            if (response.body.code === 200) {
              if (response.body.content && response.body.content.length > 0) {
                let mapping = response.body.content.map(obj => {
                  return {
                    'title': obj.trade_name.replace(/'/g, ''),
                    'value': obj.trade_name.replace(/'/g, '')
                  }
                })
                self.campaignAssetsListMerchant = mapping
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Get Merchant, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop'),
      submitCreatePost: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}, statusTab = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('UPDATE POST CAMPAIGN')
          }).then(response => {
            if (response.body.code === 200) {
              self.allSuccess = true
              let titleFlash = ''
              if (statusTab === 'onCreateDetail') {
                self.campaignAssets = response.body.content || ''
                titleFlash = 'Success Create Campaign'
              } else if (statusTab === 'onEditDetail') {
                self.campaignsDetailsStatus = response.body.content.status || ''
                titleFlash = 'Success Update Detail Campaign'
              } else if (statusTab === 'onEditAssetType') {
                self.campaignAssets = response.body.content || null
                titleFlash = 'Success Update Asset Campaign'
              } else if (statusTab === 'onUploadImage') {
                self.imagesUrlS3 = response.body || null
                titleFlash = 'Success Upload Image'
              } else if (statusTab === 'onEditAsset') {
                self.$router.push('/campaigns/list')
                titleFlash = 'Success Update Asset Campaign'
              }
              self.flash(titleFlash, 'success', {
                timeout: 3000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Campaign Create, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      getDetails: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET CAMPAIGNS DETAIL')
          }).then(response => {
            if (response.body.code === 200) {
              self.campaignsDetails = response.body.content || null
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.campaignsDetails = []
              self.flash(`Information, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.campaignsDetails = null
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getAssets: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET CAMPAIGNS ASSETS')
          }).then(response => {
            if (response.body.code === 200) {
              self.campaignsAssets = response.body.content[0] || null
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.campaignsAssets = []
              self.flash(`Information, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.campaignsAssets = null
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {})
    }
  }
}

export default CampaignsMixins
