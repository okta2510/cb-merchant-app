
import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth'
Vue.use(Vuency)
// ###
let OutletPhotosMixin = {
  name: 'OutletPhotosMixin',
  mixins: [authMixin],
  methods: {
    countSuccessUpload: function (arr) {
    }
  },
  tasks (t) {
    return {
      getListPhoto: t(function * (endpoint = '', resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('UPLOAD PHOTO')
          }).then(response => {
            // ###
            if (response.body.code === 200) {
              if (self.active.value === 'menu') {
                self.$store.commit('outlet/setPhotoMenu', response.body.content)
              } else if (self.active.value === 'food') {
                self.$store.commit('outlet/setPhotoFood', response.body.content)
              } else if (self.active.value === 'ambience') {
                self.$store.commit('outlet/setPhotoAmbience', response.body.content)
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error upload photos : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      deletePhoto: t(function * (endpoint = '', resource = '', payload = [], responseMessage = 'photos was deleted successfully') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(endpoint + resource, payload, {
            headers: self.GetHeader('DELETE PHOTO')
          }).then(response => {
            // ###
            if (response.body.code === 200) {
              self.flash(`Photos was deleted successfully`, 'success', {
                timeout: 5000
              })
              self.$store.commit('outlet/resetBulk')
              // reset state
              self.onArrange = false
              // get new list photo
              self.getList()
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error deleting photos : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      setPrimary: t(function * (endpoint = '', resource = '', payload = {}, selectedObj = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(endpoint + resource, payload, {
            headers: self.GetHeader('SET PRIMARY')
          }).then(response => {
            // ###
            if (response.body.code === 200) {
              self.flash(`Set as primary photo is successfully`, 'success', {
                timeout: 5000
              })
              this.$store.commit('outlet/setPrimary', selectedObj)
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error set primary photos : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      updatePhoto: t(function * (endpoint = '', resource = '', payload = []) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(endpoint + resource, payload, {
            headers: self.GetHeader('UPDATE PHOTO')
          }).then(response => {
            // ###
            if (response.body.code === 200) {
              self.flash(`Photos was updated successfully`, 'success', {
                timeout: 5000
              })
              self.$store.commit('outlet/resetBulk')
              // reset state
              self.onArrange = false
              // get new list photo
              self.getList()
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error updating photos : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      uploadPhoto: t(function * (endpoint = '', resource = '', payload = []) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(endpoint + resource, payload, {
            headers: self.GetHeader('UPLOAD PHOTO')
          }).then(response => {
            // ###
            if (response.body.code === 200) {
              $('#UploadPhoto').modal('hide')
              self.flash(`${response.body.content.file.length} photos was uploaded successfully`, 'success', {
                timeout: 5000
              })
              // ###
              if (self.active.value === 'menu') {
                self.$store.commit('outlet/setPhotoMenu', response.body.content)
              } else if (self.active.value === 'food') {
                self.$store.commit('outlet/setPhotoFood', response.body.content)
              } else if (self.active.value === 'ambience') {
                self.$store.commit('outlet/setPhotoAmbience', response.body.content)
              }
              // reset state
              self.bulkUploadPhotos = []
              self.onArrange = false
              // get new list photo
              self.getList()
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error uploading photos : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        })
    }
  }
}

export default OutletPhotosMixin
