import * as dummy from '@/utils/dummy'
import Vue from 'vue'
import Vuency from 'vuency'
Vue.use(Vuency)

let outletCreateEditMixin = {
  name: 'outletCreateEditMixin',
  data: function () {
    return {
      merchantPubId: '',
      listAllCategories: [],
      query: {
        size: 10000000,
        pagination: 1,
        order: 'create_ts',
        orderAttr: 'desc'
      },
      merchantSelected: null,
      listMerchants: [],
      listOutletStatus: [
        { name: 'INITIATED', value: 'INITIATED' },
        { name: 'INVISIBLE', value: 'INVISIBLE' },
        { name: 'ACTIVE', value: 'ACTIVE' },
        { name: 'TEMPORARILY CLOSED', value: 'TEMPORARILY CLOSED' },
        { name: 'DEACTIVATED', value: 'DEACTIVATED' },
        { name: 'TERMINATED', value: 'TERMINATED' }

      ],
      listPaymentTransaction: dummy.PaymentTransaction,
      listSettlements: [],
      // ###
      priceRange: [],
      dishes: [],
      features: [],
      smoking: [],
      paymentInterface: [],
      mealServed: [],
      services: [],
      wifi: [],
      cuisines: [],
      alcohol: [],
      seating: [],
      parking: [],
      deliveryInstructions: [],
      outletOptions: [],
      // start list outlet category
      listPriceRange: [],
      listDishes: [],
      listFeatures: [],
      listSmoking: [],
      listOutletOptions: [],
      listDeliveryInstructions: [],
      listPaymentInterface: [],
      listMealServed: [],
      listServices: [],
      listWifi: [],
      listCuisines: [],
      listAlcohol: [],
      listSeating: [],
      listParking: [],
      // end list outlet category
      // start location
      listOfProvince: [],
      listOfCities: [],
      listOfDistrict: [],
      listOfArea: [],
      listOfSubdistrict: [],
      listOfBuilding: [],
      // end location
      API_version: process.env.VUE_APP_API,
      API_MERCHANTS: 'merchant/status?status=ACTIVE,INITIATED',
      API_INTERFACE_CAT: 'merchant/content-categories',
      API_SETTLEMENTS: 'settlement-accounts',
      API_LOCATION: 'locations/'
    }
  },
  created: function () {
    this.getMerchants.run(this.query, process.env.VUE_APP_API, this.API_MERCHANTS)
    this.getPaymentCategory.run(process.env.VUE_APP_API, this.API_INTERFACE_CAT)
  },
  computed: {
    API_EDIT_OUTLET: function () {
      return `outlets/update`
    },
    API_DETAIL_OUTLET: function () {
      return `outlets/detail/${this.$route.params.pubId}`
    },
    API_CREATE_OUTLET: function () {
      return `merchants/${this.merchantPubId}/outlets`
    },
    API_INTERFACE: function () {
      return this.paymentInterface ? `merchant/contents/${this.paymentInterface[0].categoryPubId}` : ''
    },
    API_PRICE: function () {
      let arr = this.priceRange[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_DISHES: function () {
      let arr = this.dishes[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_FEATURES: function () {
      let arr = this.features[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_SMOKING: function () {
      let arr = this.smoking[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_MEAL: function () {
      let arr = this.mealServed[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_SERVICES: function () {
      let arr = this.services[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_WIFI: function () {
      let arr = this.wifi[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_Cuisines: function () {
      let arr = this.cuisines[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_ALCOHOL: function () {
      let arr = this.alcohol[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_SEATING: function () {
      let arr = this.seating[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_PARKING: function () {
      let arr = this.parking[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_DELIVERY: function () {
      let arr = this.deliveryInstructions[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    },
    API_OPTIONS: function () {
      let arr = this.outletOptions[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    }
  },
  methods: {
    onGetOutletDetail: function () {
      this.getOutletDetail.run(process.env.VUE_APP_API_V2, this.API_DETAIL_OUTLET)
    },
    onSubmitData: function (payload) {
      payload.additionalInfoOutlet.map(key => {
        key.categoryPubID = key.categoryPubId
      })
      if (this.$route.params.pubId) {
        payload.outlet.pubId = this.$route.params.pubId
        payload.managerInfo.pubId = this.$route.params.pubId
        this.editOutlet.run(process.env.VUE_APP_API_V2, this.API_EDIT_OUTLET, payload)
      } else {
        this.createOutlet.run(process.env.VUE_APP_API_V2, this.API_CREATE_OUTLET, payload)
      }
    },
    onGetLocation: function (query, state) {
      this.getLocation.run(process.env.VUE_APP_API, this.API_LOCATION, query, state)
    },
    onSelectingMerchant: function (obj) {
      this.merchantPubId = obj.pubId
      let query = { ...this.query, ...{ merchantPubId: obj.pubId } }
      this.getSettlements.run(query, process.env.VUE_APP_API, this.API_SETTLEMENTS)
    },
    formatPrice: function (num) {
      if (num) {
        var str = num.toString().split('.')
        if (str[0].length >= 4) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.')
        }
        return 'Rp ' + str
      } else {
        return 0
      }
    }
  },
  tasks (t) {
    return {
      getOutletDetail: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET OUTLET DETAIL')
          }).then(response => {
            if (response.body.code === 200) {
              self.detailInfo = response.body.content
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get outlet detail, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue'),
      createOutlet: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('CREATE OUTLET')
          }).then(response => {
            if (response.body.code === 200) {
              self.$router.push('/business-setup/outlets/list')
              self.flash('Create outlet success', 'success', {
                timeout: 5000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.code || 'Error'} : ${response.body.message || 'Create outlet'}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      editOutlet: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('CREATE OUTLET')
          }).then(response => {
            if (response.body.code === 200) {
              self.$router.push('/business-setup/outlets/list')
              self.flash(`Outlet ${payload.outlet.outletName} has been successfully update`, 'success', {
                timeout: 5000,
                beforeDestroy: function () {
                }
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.code || 'Error'} : ${response.body.message || 'Edit Outlet'}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getLocation: t(function * (endpoint = process.env.VUE_APP_API, resource = '', query = '', stateName = '') {
        let self = this
        if (this.GetAuth()) {
          self.$http.get(`${endpoint}${resource}${query}`, {
            headers: this.GetHeader('Create Location')
          }).then(response => {
            if (response.body.code === 200) {
              this[stateName] = response.body.content || []
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              this[stateName] = []
              self.flash(`Error get locations list, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      getMerchants: t(function * (queryParams, endpoint = this.API_version, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST MERCHANTS')
          }).then(response => {
            if (response.body.code === 200) {
              let mapping = response.body.content.map(obj => {
                return {
                  name: obj.trade_name,
                  pubId: obj.merchant_pubid
                }
              })
              self.listMerchants = mapping
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get merchants list, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop'),
      getSettlements: t(function * (queryParams, endpoint = this.API_version, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST'),
            params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              this.listSettlements = response.body.content || []
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get settlements list, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop'),
      getPaymentCategory: t(function * (endpoint = this.API_version, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST')
            // params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.listAllCategories = response.body.content
              // ###
              let priceRange = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'price range')
              self.priceRange = priceRange
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_PRICE, 'listPriceRange', 'price range')
              // ###
              let paymentInterface = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'payment interface')
              self.paymentInterface = paymentInterface
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_INTERFACE, 'listPaymentInterface')
              // ###
              let dishes = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'dishes')
              self.dishes = dishes
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_DISHES, 'listDishes')
              // ###
              let features = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'features')
              self.features = features
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_FEATURES, 'listFeatures')
              // ###
              let smoking = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'smoking')
              self.smoking = smoking
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_SMOKING, 'listSmoking')
              // ###
              let mealServed = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'meal served')
              self.mealServed = mealServed
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_MEAL, 'listMealServed')
              // ###
              let services = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'service')
              self.services = services
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_SERVICES, 'listServices')
              // ###
              let wifi = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'wi-fi')
              self.wifi = wifi
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_WIFI, 'listWifi')
              // ###
              let cuisines = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'cuisine')
              self.cuisines = cuisines
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_Cuisines, 'listCuisines')
              // ###
              let alcohol = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'alcohol')
              self.alcohol = alcohol
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_ALCOHOL, 'listAlcohol')
              // ###
              let seating = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'seating')
              self.seating = seating
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_SEATING, 'listSeating')
              // ###
              let parking = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'parking')
              self.parking = parking
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_PARKING, 'listParking')
              // ###
              let deliveryInstructions = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'delivery instruction')
              self.deliveryInstructions = deliveryInstructions
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_DELIVERY, 'listDeliveryInstructions')
              // ###
              let outletOptions = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'join type')
              self.outletOptions = outletOptions
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_OPTIONS, 'listOutletOptions')
              // ###
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get content categories, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop'),
      getCategoryDetail: t(function * (endpoint = this.API_version, resource = '', state, status = '') {
        let self = this
        if (this.GetAuth() && resource) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST')
          }).then(response => {
            if (response.body.code === 200) {
              if (response.body.content) {
                let mapping = response.body.content.map(obj => {
                  var statusName = obj.contentValue
                  if (status === 'price range') {
                    // statusName = '100.000'
                    statusName = this.formatPrice(statusName)
                    // alert(statusName)
                  }
                  return {
                    name: statusName || '-',
                    pubId: obj.contentPubId
                  }
                })
                self[state] = mapping
              } else {
                self[state] = []
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get content category ${state}, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        } else {
          self[state] = []
        }
      })
        .flow('enqueue')
    }
  }
}

export default outletCreateEditMixin
