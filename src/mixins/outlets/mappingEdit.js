import Vue from 'vue'
import Vuency from 'vuency'
Vue.use(Vuency)

let mappingEdit = {
  name: 'mappingEdit',
  data () {
    return {
      mappingEditDone: true,
      selectedHours: [],
      loadingMapping: false
    }
  },
  created () {
    if (this.$route.params.pubId) {
      this.loadingMapping = true
    }
  },
  watch: {
    detailInfo: async function () {
      await this.mappingOutlet()
      await this.mappingManager()
      await this.mappingMedia()
      await this.mappingHours()
      await this.mappingPaymentTransaction()
      await this.mappingOutletOptions()
      await setTimeout(async (x) => {
        await this.mappingAdditional()
        await this.mappingLocation()
        await this.mappingSettlement()
        this.loadingMapping = false
      }, 1000)
    }
  },
  methods: {
    mappingOutletOptions: function () {
      if (this.outlet.outletOptions) {
        this.outlet.outletOptions = this.detailInfo.outlet && this.detailInfo.outlet.outletOptions ? this.detailInfo.outlet.outletOptions : ''
        this.deliveryWa = this.detailInfo.delivery && this.detailInfo.delivery.waNumber ? this.detailInfo.delivery.waNumber : ''
        this.sectionLabel = this.detailInfo.delivery && this.detailInfo.delivery.menus[0] ? this.detailInfo.delivery.menus[0].category : ''
        this.sectionLabel2 = this.detailInfo.delivery && this.detailInfo.delivery.menus[1] ? this.detailInfo.delivery.menus[1].category : ''

        if (this.detailInfo.delivery && this.detailInfo.delivery.menus[0] && this.detailInfo.delivery.menus[0].menu.length > 0) {
          let arr = this.detailInfo.delivery.menus[0].menu
          let string = ''
          arr.forEach((el, index) => {
            if (el.length - 1 > index) {
              string = string + el + '\n'
            } else {
              string = string + el
            }
          })
          this.sectionMenu = string
        }

        if (this.detailInfo.delivery && this.detailInfo.delivery.menus[1] && this.detailInfo.delivery.menus[1].menu.length > 0) {
          let arr = this.detailInfo.delivery.menus[1].menu
          let string = ''
          arr.forEach((el, index) => {
            if (el.length - 1 > index) {
              string = string + el + '\n'
            } else {
              string = string + el
            }
          })
          this.sectionMenu2 = string
        }

        this.deliveryInstruction = this.detailInfo.delivery && this.detailInfo.delivery.deliveryInstruction ? this.detailInfo.delivery.deliveryInstruction : ''
      }
    },
    mappingPaymentTransaction: function () {
      let obj = {}
      if (this.detailInfo.outlet && this.detailInfo.outlet.isOnline === true) {
        obj = {
          name: 'online',
          value: true
        }
      } else {
        obj = {
          name: 'offline',
          value: false
        }
      }
      this.tempPaymentTransaction = obj
    },
    mappingHours: async function () {
      let arr = this.detailInfo.openingHoursOutlet
      if (arr) {
        await arr.forEach((day, index) => {
          this.days.forEach((state, index2) => {
            if (day.dayOfWeeks.toLowerCase() === state.name) {
              this.days[index2].val = true
            }
          })
        })
      }
      this.hourCollections = this.detailInfo.openingHoursOutlet
      this.selectedHours = this.detailInfo.openingHoursOutlet
    },
    mappingAdditional: function () {
      let arr = this.detailInfo.additionalInfoOutlet
      let arrName = ['wi-fi', 'price range', 'smoking', 'seating', 'dishes', 'alcohol', 'service', 'parking', 'meal served', 'features', 'cuisine']
      let stateName = ['tempWifi', 'tempPriceRange', 'tempSmoking', 'tempSeating', 'tempDishes', 'tempAlcohol', 'tempServices', 'tempParking', 'tempMealServed', 'tempFeatures', 'tempCuisines']
      let wichMultiSelection = ['dishes', 'meal served', 'features', 'service', 'parking', 'cuisine']
      // ### mapping
      arrName.forEach((element, index) => {
        let temp = arr.filter(x => x.categoryValue.toLowerCase() === element)
        if (temp.length > 0) {
          if (wichMultiSelection.includes(element)) {
            // ### for multiple select additional info
            let newArr = []
            temp[0].content.forEach((category, indexCat) => {
              let { pubId, name } = category
              let obj = {
                name: name,
                pubId: pubId
              }
              newArr.push(obj)
            })
            this[stateName[index]] = newArr
          } else {
            // ###for single select additional info
            let { pubId, name, tooltipDesc, subDesc } = temp[0].content[0]
            let nameData = ''
            if (element === 'price range') {
              nameData = this.formatPrice(name)
            } else {
              nameData = name
            }
            let obj = {
              name: nameData,
              element: element,
              pubId: pubId
            }
            if (element === 'price range') {
              obj.tooltipDesc = tooltipDesc
              obj.subDesc = subDesc
              this[stateName[index]] = obj
              this.tempSubDesc = subDesc
              this.tempTipDesc = tooltipDesc
            } else {
              this[stateName[index]] = obj
            }
          }
        }
      })
    },
    mappingManager: function () {
      this.managerInfo = this.detailInfo.managerInfo
    },
    mappingMedia: function () {
      this.media = this.detailInfo.media
    },
    mappingSettlement: function () {
      this.tempSettlementAccount = this.detailInfo.settlementAccount
    },
    mappingOutlet: async function () {
      this.outlet = Object.assign(this.outlet, this.detailInfo.outlet)
      this.selectingMerchant()
    },
    mappingLocation: async function () {
      this.location = Object.assign(this.location, this.detailInfo.outlet.location)
      let { province, city, district, subdistrict, building, area } = this.detailInfo.outlet.location
      this.location.province = province || { name: '', id: 0 }
      await this.selectLocation('city', 'province', 'listOfCities')
      this.location.city = city || { name: '', id: 0 }
      await this.selectLocation('district', 'city', 'listOfDistrict')
      this.location.district = district || { name: '', id: 0 }
      await this.selectLocation('subdistrict', 'district', 'listOfSubdistrict')
      this.location.subdistrict = subdistrict || { name: '', id: 0 }
      await this.selectLocation('building', 'subdistrict', 'listOfBuilding')
      this.location.building = building || { name: '', id: 0 }
      this.location.area = area || { name: '', id: 0 }
    },
    formatPrice: function (num) {
      if (num) {
        var str = num.toString().split('.')
        if (str[0].length >= 4) {
          str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1.')
        }
        return 'Rp ' + str
      } else {
        return 0
      }
    }
  }
}

export default mappingEdit
