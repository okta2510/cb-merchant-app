let watcherCategory = {
  name: 'watcherCategory',
  data: function () {
    return {
    }
  },
  computed: {
    // ['price range', 'dishes', 'features', 'smoking', 'meal served', 'service', 'wi-fi', 'cuisines', 'alcohol', 'seating', 'parking']
  },
  watch: {
    tempTipDesc: function () {
      let funcInclude = (x) => x.categoryValue.toLowerCase() === 'price range'
      let arr = this.additionalInfoOutlet.filter(funcInclude)
      let index = this.additionalInfoOutlet.findIndex(funcInclude)
      if (arr.length > 0) {
        arr[0].content[0].tooltipDesc = this.tempTipDesc
        this.additionalInfoOutlet[index] = arr[0]
      }
    },
    tempSubDesc: function () {
      let funcInclude = (x) => x.categoryValue.toLowerCase() === 'price range'
      let arr = this.additionalInfoOutlet.filter(funcInclude)
      let index = this.additionalInfoOutlet.findIndex(funcInclude)
      console.log(arr)
      if (arr.length > 0) {
        arr[0].content[0].subDesc = this.tempSubDesc
        this.additionalInfoOutlet[index] = arr[0]
      }
    },
    tempPriceRange: async function () {
      await this.checkValue('tempPriceRange', 'object')
      let funcExclude = (x) => x.categoryValue.toLowerCase() !== 'price range'
      let funcInclude = (x) => x.categoryValue.toLowerCase() === 'price range'
      let info = this.listAllCategories.filter(x => x.categoryValue.toLowerCase() === 'price range')
      let exclude = this.additionalInfoOutlet.filter(funcExclude)
      let include = this.additionalInfoOutlet.filter(funcInclude)
      let indexInclude = this.additionalInfoOutlet.findIndex(funcInclude)
      let content = []
      // ###
      if (this.tempPriceRange === null) {
        this.tempSubDesc = ''
        this.tempTipDesc = ''
        this.additionalInfoOutlet = exclude
      } else {
        content.push(this.tempPriceRange)
        if (include.length > 0) {
          this.additionalInfoOutlet[indexInclude] = { ...info[0], ...{ content: content } }
        } else {
          this.additionalInfoOutlet.push({ ...info[0], ...{ content: content } })
        }
      }
    },
    tempServices: async function () {
      await this.checkValue('tempServices')
      this.addMultiCategory('service', 'tempServices')
    },
    tempParking: async function () {
      await this.checkValue('tempParking')
      this.addMultiCategory('parking', 'tempParking')
    },
    tempMealServed: async function () {
      await this.checkValue('tempMealServed')
      this.addMultiCategory('meal served', 'tempMealServed')
    },
    tempSmoking: async function () {
      await this.checkValue('tempSmoking')
      this.addSingleCategory('smoking', 'tempSmoking')
    },
    tempSeating: async function () {
      await this.checkValue('tempSeating')
      this.addSingleCategory('seating', 'tempSeating')
    },
    tempWifi: async function () {
      await this.checkValue('tempWifi')
      this.addSingleCategory('wi-fi', 'tempWifi')
    },
    tempCuisines: async function () {
      await this.checkValue('tempCuisines')
      this.addMultiCategory('cuisine', 'tempCuisines')
    },
    tempFeatures: async function () {
      await this.checkValue('tempFeatures')
      this.addMultiCategory('features', 'tempFeatures')
    },
    tempAlcohol: async function () {
      await this.checkValue('tempAlcohol')
      this.addSingleCategory('alcohol', 'tempAlcohol')
    },
    tempDishes: async function () {
      await this.checkValue('tempDishes')
      this.addMultiCategory('dishes', 'tempDishes')
    }
  },
  methods: {
    checkValue: function (state) {
      if (this[state]) {
        if (this[state].length === 0) {
          this[state] = null
        }
      } else {
        this[state] = null
      }
    },
    addSingleCategory: function (params, state) {
      console.log(params, state)
      let funcExclude = (x) => x.categoryValue.toLowerCase() !== params
      let funcInclude = (x) => x.categoryValue.toLowerCase() === params
      let info = this.listAllCategories.filter(x => x.categoryValue.toLowerCase() === params)
      let exclude = this.additionalInfoOutlet.filter(funcExclude)
      let include = this.additionalInfoOutlet.filter(funcInclude)
      let indexInclude = this.additionalInfoOutlet.findIndex(funcInclude)
      let content = []
      // ###
      if (this[state] === null) {
        this.additionalInfoOutlet = exclude
      } else {
        content.push(this[state])
        if (include.length > 0) {
          this.additionalInfoOutlet[indexInclude] = { ...info[0], ...{ content: content } }
        } else {
          this.additionalInfoOutlet.push({ ...info[0], ...{ content: content } })
        }
      }
    },
    addMultiCategory: function (params, state) {
      let funcExclude = (x) => x.categoryValue.toLowerCase() !== params
      let funcInclude = (x) => x.categoryValue.toLowerCase() === params
      let info = this.listAllCategories.filter(x => x.categoryValue.toLowerCase() === params)
      let exclude = this.additionalInfoOutlet.filter(funcExclude)
      let include = this.additionalInfoOutlet.filter(funcInclude)
      let indexInclude = this.additionalInfoOutlet.findIndex(funcInclude)
      let content = []
      // ###
      if (this[state] === null) {
        this.additionalInfoOutlet = exclude
      } else {
        content = [...content, ...this[state]]
        if (include.length > 0) {
          this.additionalInfoOutlet[indexInclude] = { ...info[0], ...{ content: content } }
        } else {
          this.additionalInfoOutlet.push({ ...info[0], ...{ content: content } })
        }
      }
    },
    addToBucket: function () {},
    removeToBucker: function () {}
  }
}

export default watcherCategory
