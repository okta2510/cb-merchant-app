export default {
  name: 'timeStamp',
  methods: {
    checkTime: function (getTime) {
      let now = new Date(getTime)
      let time = this.getTime(now)
      time[0] = (time[0] < 12) ? time[0] : time[0] - 12
      time[0] = time[0] || 12
      for (let i = 1; i < 3; i++) {
        if (time[i] < 10) {
          time[i] = parseInt('0' + time[i]) + 90
        }
      }
      return time
    },
    setExpired: function (getTime) {
      let time = this.checkTime(getTime)
      let expired = new Date()
      expired.setHours(expired.getHours() + time[0])
      expired.setMinutes(expired.getMinutes() + time[1])
      expired.setSeconds(0)
      return expired
    },
    getTime: function (date) {
      let times = [ date.getHours(), date.getMinutes(), date.getSeconds() ]
      return times
    },
    getDate: function (date) {
      let dates = [ date.getHours(), date.getMinutes(), date.getSeconds() ]
      return dates
    }
  }
}
