import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
import * as dummy from '@/utils/dummy'
Vue.use(Vuency)

export default {
  name: 'PromotionCreateEditMixin',
  data () {
    return {
      detailInfo: null,
      listBanner: [],
      listRedemptionUnit: dummy.listRedemptionUnit,
      listRedemptionGroup: dummy.listRedemptionGroup,
      listCashbacUnit: dummy.listCashbacUnit,
      listCashbacSource: dummy.listCashbacSource,
      API_CREATE: 'promos/create',
      API_BANNERS: 'cms/campaigns',
      allSuccess: false
    }
  },
  mixins: [authMixin],
  computed: {
    API_EDIT: function () {
      return `promos/${this.id || null}/update`
    },
    API_DETAIL: function () {
      return `promos/${this.id}`
    }
  },
  tasks (t) {
    return {
      getEditDetail: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        self.$http.get(`${endpoint}${resource}`, {
          headers: this.GetHeader('VIEW DETAIL PROMO')
        }).then(response => {
          if (response.body.code === 200) {
            self.detailInfo = response.body.content
          } else if (response.body.code === 8) {
            self.GoLogout()
          } else {
            self.flash(`Failed detail of promo, try again later.`, 'error', {
              timeout: 5000,
              beforeDestroy () {
                self.$router.push('/promo/list')
              }
            })
          }
        }, (err) => {
          self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
            timeout: 10000
          })
          self.$router.push('/promo/list')
        })
      })
        .flow('enqueue', { maxRunning: 1 }),
      getListBanner: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint + resource}`, {
            headers: self.GetHeader('GET LIST BANNER')
          }).then(response => {
            if (response.body.code === 200) {
              // action after submitted
              if (response.body.content && response.body.content.length > 0) {
                self.listBanner = response.body.content
              } else {
                self.listBanner = []
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Get Banner, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      submitCreate: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('CREATE PROMOTION')
          }).then(response => {
            if (response.body.code === 200) {
              self.allSuccess = true
              self.flash('Success Create Promotion', 'success', {
                timeout: 3000,
                beforeDestroy () {
                  self.$router.push({
                    name: 'PromotionList'
                  })
                }
              })
            } else if (response.body.code === 206 && response.body.pubId !== '') {
              self.flash(response.body.message, 'success', {
                timeout: 3000,
                beforeDestroy () {
                  self.$router.push({
                    name: 'PromotionEdit',
                    params: {
                      id: response.body.content.pubId,
                      text: response.body.content.promoName
                    }
                  })
                }
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Promotion Create, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      submitEdit: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('EDIT PROMOTION')
          }).then(response => {
            if (response.body.code === 200) {
              self.allSuccess = true
              self.flash('Success Edit Promotion', 'success', {
                timeout: 3000,
                beforeDestroy () {
                  self.$router.push({
                    name: 'PromotionList'
                  })
                }
              })
            } else if (response.body.code === 206 && response.body.pubId !== '') {
              self.flash(response.body.message, 'success', {
                timeout: 3000,
                beforeDestroy () {
                  self.$router.push({
                    name: 'PromotionEdit',
                    params: {
                      id: response.body.content.pubId,
                      text: response.body.content.promoName
                    }
                  })
                }
              })
              // action after edited
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Promotion Edit : ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 })
    }
  }
}
