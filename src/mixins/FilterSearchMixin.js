export default {
  name: 'FilterSearchMixin',
  data: function () {
    return {
    }
  },
  methods: {
    onDateRangeChanged: function (picker, tag, obj) {
      let dateStart = tag.getAttribute('target') + '_start'
      let dateEnd = tag.getAttribute('target') + '_end'
      this.querySearch[dateStart] = obj.start
      this.querySearch[dateEnd] = obj.end
    },
    onSelectingList (value, target) {
      this.querySearch[target] = value
    }
  }
}
