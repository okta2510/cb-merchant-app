import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
Vue.use(Vuency)
const LocationMixin = {
  data () {
    return {
    }
  },
  mixins: [authMixin],
  tasks (t) {
    return {
      getData: t(function * (endpoint = process.env.VUE_APP_API, resource = '', query = '', stateName = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}${query}`, {
            headers: this.GetHeader('Create Location')
          }).then(response => {
            if (response.body.code === 200) {
              if (response.body.content !== null) {
                this[stateName] = response.body.content
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get location, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      submitData: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: this.GetHeader('Create Location')
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`Success create new location`, 'success', {
                timeout: 5000
              })
              self.$router.push('/content-settings/locations/list')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error create location, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 })
    }
  }
}

export default LocationMixin
