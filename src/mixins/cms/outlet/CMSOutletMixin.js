import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
Vue.use(Vuency)

const CMSOutletMixin = {
  name: 'CMSOutletMixin',
  mixins: [authMixin],
  tasks (t, { timeout }) {
    return {
      createOutletContent: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('GET CONTENT CATEGORIES')
          }).then(response => {
            if (response.body.code === 200) {
              self.flash('Create content success', 'success', {
                timeout: 5000,
                beforeDestroy () {
                  self.$router.push('/content-settings/outlets/list')
                }
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Create content, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getOutletCategory: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET CONTENT CATEGORIES')
          }).then(response => {
            if (response.body.code === 200) {
              self.listOfCategories = response.body.content
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Failed get content list category', 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
    }
  }
}

export default CMSOutletMixin
