import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
Vue.use(Vuency)

let SettlementSubmit = {
  name: 'SettlementSubmit',
  mixins: [authMixin],
  tasks (t, { timeout }) {
    return {
      getInfo: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET SETTLEMENT INFO')
          }).then(response => {
            if (response.body.code === 200) {
              self.settlementInformation = response.body.content || null
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get settlement info, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.settlementInformation = null
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getSum: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET SETTLEMENT INFO')
          }).then(response => {
            if (response.body.code === 200) {
              self.settlementSummary = response.body.content || null
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get settlement summary, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.settlementSummary = null
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getTrx: t(function * (endpoint = process.env.VUE_APP_API, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}`, {
            headers: self.GetHeader('GET SETTLEMENT INFO')
          }).then(response => {
            if (response.body.code === 200) {
              self.transactionSummary = response.body.content || []
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get transaction summary, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.transactionSummary = []
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      submitSettlement: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = null, type = 'submit') {
        let self = this
        type = type.toUpperCase()
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader(`${type} SETTLEMENT`)
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`${response.body.message || `Success ${type} settlement`}`, 'success', {
                timeout: 5000
              })
              self.$router.push('/settlements/regular/list')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.message || `Error ${type} settlement `}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue'),
      approveSettlement: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = null, type = 'submit') {
        let self = this
        type = type.toUpperCase()
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader(`${type} SETTLEMENT`)
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`${response.body.message || `Success ${type} settlement`}`, 'success', {
                timeout: 5000
              })
              self.$router.push('/settlements/regular/list')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.message || `Error ${type} settlement `}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue'),
      rejectSettlement: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = null, type = 'submit') {
        let self = this
        type = type.toUpperCase()
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader(`${type} SETTLEMENT`)
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`${response.body.message || `Success ${type} settlement`}`, 'success', {
                timeout: 5000
              })
              self.$router.push('/settlements/regular/list')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.message || `Error ${type} settlement `}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue'),
      cancelSettlement: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = null, type = 'submit') {
        let self = this
        type = type.toUpperCase()
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader(`${type} SETTLEMENT`)
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`${response.body.message || `Success ${type} settlement`}`, 'success', {
                timeout: 5000
              })
              self.$router.push('/settlements/regular/list')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`${response.body.message || `Error ${type} settlement `}`, 'error', {
                timeout: 5000
              })
            }
          }, err => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      submitPaid: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = null) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('WITHDRAWAL PAID')
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(`${response.body.message || 'Success withdrawal paid'}`, 'success', {
                timeout: 5000
              })
              self.paidDate = 0
              self.$router.push('/withdrawal/regular')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error withdrawal paid, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {})
    }
  }
}

export default SettlementSubmit
