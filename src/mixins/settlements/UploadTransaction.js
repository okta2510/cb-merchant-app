import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
Vue.use(Vuency)

let settlementMixin = {
  name: 'uploadTransaction',
  mixins: [authMixin],
  tasks (t, { timeout }) {
    return {
      getMerchantList: t(function * (endpoint = process.env.VUE_APP_API, resource = '', query = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}${query}`, {
            headers: self.GetHeader('GET MERCHANTS')
          }).then(response => {
            if (response.body.code === 200) {
              self.merchantList = response.body.content || []
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Create content, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      getSettlementList: t(function * (endpoint = process.env.VUE_APP_API, resource = '', query = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}?${query}`, {
            headers: self.GetHeader('GET SETTLEMENTS')
          }).then(response => {
            if (response.body.code === 200) {
              self.settlementList = response.body.content || []
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Create content, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {}),
      downloadTransaction: t(function * (endpoint = process.env.VUE_APP_API, resource = '', query = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(`${endpoint}${resource}?${query}`, {
            headers: self.GetHeader('DOWNLOAD TRANSACTIONS')
          }).then(response => {
            if (response.body.code === 200) {
              self.flash(response.body.content, 'success', {
                timeout: 5000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error Create content, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {})
    }
  }
}
export default settlementMixin
