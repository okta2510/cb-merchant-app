
import Vue from 'vue'
import Vuency from 'vuency'
Vue.use(Vuency)
// ini dipakai disemua table list pastiin pada saat update sesuatu disini update juga callernya
export default {
  name: 'TableListMixinMulti',
  methods: {
    onFilterSubmit: function (query, endpoint = this.API_endpoint) {
      let request = this.getParams
      let obj = {}
      if (query.status && query.status === 'ALL') {
        delete query.status
      }
      Object.assign(obj, query)
      Object.assign(obj, request)
      this.getDataList.run(obj, endpoint)
    },
    onPaginationClick: function (query, endpoint = this.API_endpoint) {
      let self = this
      let request = this.getParams
      let obj = {}
      if (query.status && query.status === 'ALL') {
        delete query.status
      }
      Object.keys(request).forEach((key, index) => {
        if (key === self.paginationName) {
          request.pagination = request[key]
          delete request[self.paginationName]
        }
      })
      Object.assign(obj, query)
      Object.assign(obj, request)
      this.getDataList.run(obj, endpoint)
    },
    refreshQuery: function () {
      let query = this.$route.query
      let reset = { [this.paginationName]: '1' }
      query = { ...query, ...reset }
      this.$router.push({
        query: query
      })
    },
    formatThousand: function (num) {
      return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
  },
  tasks (t) {
    return {
      getDataList: t(function * (queryParams, endpoint = this.API_endpoint) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + self.APIURL, {
            headers: self.GetHeader('VIEW LIST'),
            params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.results = response.body.content
              self.params[this.paginationName] = response.body.pageNum
              self.totalPage = response.body.totalPages
              self.totalRows = response.body.totalRows
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error get list : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      deleteListID: t(function * (endpoint = this.API_version, resource = '', payload) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('Delete Outlet Content')
          }).then(response => {
            if (response.body.code === 200) {
              self.initialLoadData()
              self.flash('Success delete', 'success', {
                timeout: 5000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error delete : ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        })
    }
  }
}
