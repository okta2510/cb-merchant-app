import Vue from 'vue'
import Vuency from 'vuency'
import authMixin from '@/mixins/auth.js'
import * as dummy from '@/utils/dummy'
Vue.use(Vuency)

export default {
  name: 'CreateEditMixin',
  data () {
    return {
      detailInfo: null,
      API_DETAIL: 'merchant/',
      API_TAGS: 'cms/tags',
      API_BANKS: 'cms/banks',
      API_MERCHANTS: 'merchants/list',
      API_UPLOAD_IMG: 'merchant/media/upload',
      API_CREATE: 'merchant/create',
      API_INTERFACE_CAT: 'merchant/content-categories',
      merchantOptions: [],
      listMerchantOptions: [],
      listAllCategories: [],
      listTags: [],
      listBanks: [],
      listMerchants: [],
      listLocations: {},
      listProvince: [],
      listStatus: dummy.merchantListStatus,
      listBusinessType: dummy.merchantListBusinessType
    }
  },
  created: function () {
    this.getPaymentCategory.run(process.env.VUE_APP_API, this.API_INTERFACE_CAT)
  },
  computed: {
    API_OPTIONS: function () {
      let arr = this.merchantOptions[0]
      return arr ? `merchant/contents/${arr.categoryPubId}` : ''
    }
  },
  mixins: [authMixin],
  tasks (t) {
    return {
      getCategoryDetail: t(function * (endpoint = this.API_version, resource = '', state, status = '') {
        let self = this
        if (this.GetAuth() && resource) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST')
          }).then(response => {
            if (response.body.code === 200) {
              if (response.body.content) {
                let mapping = response.body.content.map(obj => {
                  var statusName = obj.contentValue
                  if (status === 'average cost') {
                    // statusName = '100.000'
                    statusName = this.formatPrice(statusName)
                    // alert(statusName)
                  }
                  return {
                    name: statusName || '-',
                    pubId: obj.contentPubId
                  }
                })
                self[state] = mapping
              } else {
                self[state] = []
              }
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get content category ${state}, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        } else {
          self[state] = []
        }
      })
        .flow('enqueue'),
      getPaymentCategory: t(function * (endpoint = this.API_version, resource = '') {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + resource, {
            headers: self.GetHeader('GET LIST')
            // params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.listAllCategories = response.body.content
              let merchantOptions = response.body.content.filter(obj => obj.categoryValue.toLowerCase() === 'join type')
              self.merchantOptions = merchantOptions
              self.getCategoryDetail.run(process.env.VUE_APP_API, self.API_OPTIONS, 'listMerchantOptions')
              // ###
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error get content categories, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop'),
      getEditDetail: t(function * (endpoint = process.env.VUE_APP_API, resource = '', id = '') {
        let self = this
        self.$http.get(`${endpoint}${resource}${id}`, {
          headers: this.GetHeader('VIEW DETAIL')
        }).then(response => {
          if (response.body.code === 200) {
            self.detailInfo = response.body.content
          } else if (response.body.code === 8) {
            self.GoLogout()
          } else {
            self.flash(`Failed detail of merchants, try again later.`, 'error', {
              timeout: 5000,
              beforeDestroy () {
                self.$router.push('/business-setup/merchants/list')
              }
            })
          }
        }, (err) => {
          self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
            timeout: 10000
          })
        })
      })
        .flow('enqueue', { maxRunning: 1 }),
      getTags: t(function * (queryParams = '', endpoint = process.env.VUE_APP_API) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + self.API_TAGS, {
            headers: self.GetHeader('GET TAGS MERCHANT'),
            params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.listTags = response.body.content
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Failed get list of tags, try again later.`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      getBanks: t(function * (queryParams = '', endpoint = process.env.VUE_APP_API) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + self.API_BANKS, {
            headers: self.GetHeader('GET BANKS MERCHANT'),
            params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.listBanks = response.body.content
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Failed get list of banks, try again later.`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      getMerchants: t(function * (queryParams = '', endpoint = process.env.VUE_APP_API_V2) {
        let self = this
        queryParams = {
          pagination: 1,
          size: 1000,
          order: 'create_ts',
          orderAttr: 'desc'
        }
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + self.API_MERCHANTS, {
            headers: self.GetHeader('GET LIST MERCHANT'),
            params: queryParams
          }).then(response => {
            if (response.body.code === 200) {
              self.listMerchants = response.body.content
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Failed get list of merchants, try again later.`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      getLocations: t(function * () {
        let self = this
        yield self.$http.get('/utils/province.json', {
          headers: self.GetHeader('GET LOCATION MERCHANT')
        }).then(response => {
          self.listLocations = response.body
          let arr = Object.keys(response.body)
          arr.forEach((element, index) => {
            self.listProvince.push({ name: element })
          })
        }, (err) => {
          self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
            timeout: 10000
          })
        })
      })
        .flow('enqueue', { maxRunning: 1 }),
      uploadImage: t(function * (endpoint = process.env.VUE_APP_API, resource = '', obj) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, obj, {
            headers: self.GetHeader('UPLOAD IMAGE')
          }).then(response => {
            if (response.body.code === 200) {
              self.$store.commit('merchants/decrementBulkUpload')
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              this.flash(`error image upload, ${obj.fileName}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {
          let self = this
          if (self.$store.getters['merchants/getBulkUploadImage'] === 0) {
            self.$store.commit('merchants/setSuccessImage', true)
            self.flash('All your image upload successfully', 'success', {
              timeout: 2000
            })
            self.$router.push({
              name: 'MerchantList'
            })
          }
        }),
      deleteImage: t(function * (endpoint = process.env.VUE_APP_API, resource = '', obj) {
        let payload = {
          merchantPubId: this.id,
          file: obj
        }
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('DELETE IMAGE')
          }).then(response => {
            if (response.body.code === 200) {
              self.$store.commit('merchants/resetBulkDeleteImage')
              // self.flash('Bulkdelete successfully', 'success', {
              //   timeout: 2000
              // })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              this.flash(`error bulkdelete, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue')
        .onSuccess(() => {
          let self = this
          let bulkUpload = self.$store.getters['merchants/getBulkUploadImage']
          let bulkDelete = self.$store.getters['merchants/getBulkDeleteImage']
          if (bulkDelete.length === 0) {
            self.flash('Bulkdelete successfully', 'success', {
              timeout: 2000,
              beforeDestroy () {
                if (bulkUpload === 0 && bulkDelete.length === 0) {
                  self.$router.push({
                    name: 'MerchantList'
                  })
                }
              }
            })
          }
        }),
      submitCreate: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('CREATE MERCHANT')
          }).then(response => {
            if (response.body.code === 200) {
              self.$store.commit('merchants/setNewCreateSubmit', true)
              self.$store.commit('merchants/setTempMerchantObj', response.body.content)
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`error merchant create, ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 }),
      submitEdit: t(function * (endpoint = process.env.VUE_APP_API, resource = '', payload = {}) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('EDIT MERCHANT')
          }).then(response => {
            if (response.body.code === 200) {
              self.$store.commit('merchants/setNewCreateSubmit', true)
              self.$store.commit('merchants/setTempMerchantObj', response.body.content.merchant)
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`error merchant edit : ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('enqueue', { maxRunning: 1 })
    }
  }
}
