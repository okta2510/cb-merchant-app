
import Vue from 'vue'
import Vuency from 'vuency'
Vue.use(Vuency)
// ini dipakai disemua table list pastiin pada saat update sesuatu disini update juga callernya
export default {
  name: 'TableListMixin',
  methods: {
    onFilterSubmit: function (query, endpoint = this.API_endpoint) {
      let request = this.getParams
      let obj = {}
      this.exceptionalFilter.forEach(element => {
        if (query[element] && query[element].toLowerCase() === 'all') {
          delete query[element]
        }
      })
      Object.assign(obj, query)
      Object.assign(obj, request)
      this.getDataList.run(obj, endpoint)
    },
    onPaginationClick: function (query, endpoint = this.API_endpoint) {
      let request = this.getParams
      let obj = {}
      this.exceptionalFilter.forEach(element => {
        if (query[element] && query[element].toLowerCase() === 'all') {
          delete query[element]
        }
      })
      Object.assign(obj, query)
      Object.assign(obj, request)
      this.getDataList.run(obj, endpoint)
    },
    refreshQuery: function () {
      this.$router.push({
        query: {
          pagination: 1
        }
      })
    },
    formatThousand: function (num) {
      return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }
  },
  tasks (t) {
    return {
      getDataList: t(function * (queryParams, endpoint = this.API_endpoint) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.get(endpoint + self.APIURL, {
            headers: self.GetHeader('VIEW LIST'),
            params: queryParams
          }).then((response) => {
            if (response.body.code === 200) {
              self.results = response.body.content
              self.params.pagination = response.body.pageNum
              self.totalPage = response.body.totalPages
              self.totalRows = response.body.totalRows
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash('Error get list : ' + response.body.message, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
            self.results = []
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      deleteListID: t(function * (endpoint = this.API_version, resource = '', payload) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('Delete Outlet Content')
          }).then(response => {
            if (response.body.code === 200) {
              self.initialLoadData()
              self.flash('Success delete', 'success', {
                timeout: 5000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error delete : ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        }),
      submitEditBulk: t(function * (endpoint = this.API_version, resource = '', payload) {
        let self = this
        if (this.GetAuth()) {
          yield self.$http.post(`${endpoint}${resource}`, payload, {
            headers: self.GetHeader('UPDATE BULK STATUS OUTLET')
          }).then(response => {
            if (response.body.code === 200) {
              self.initialLoadData()
              self.flash('Outlet Bulk Status has been successfully update', 'success', {
                timeout: 5000
              })
            } else if (response.body.code === 8) {
              self.GoLogout()
            } else {
              self.flash(`Error delete : ${response.body.message}`, 'error', {
                timeout: 5000
              })
            }
          }, (err) => {
            self.flash(`Error : ${err.status} ${err.statusText}`, 'error', {
              timeout: 10000
            })
          })
        }
      })
        .flow('drop')
        .beforeStart(() => {
          if (this.CheckExpired()) {
            this.GoLogout()
          }
        })
    }
  }
}
