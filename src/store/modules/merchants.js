const state = {
  newCreateSubmitted: false,
  bulkUploadImage: 0,
  bulkDeleteImage: [],
  tempMerchantObj: {},
  onSuccessImage: false
}
const getters = {
  getCreateSubmitted: (state) => {
    return state.newCreateSubmitted
  },
  getBulkUploadImage: (state) => {
    return state.bulkUploadImage
  },
  getBulkDeleteImage: (state) => {
    return state.bulkDeleteImage
  },
  isSuccessImage: (state) => {
    return state.onSuccessImage
  },
  getTempMerchant: (state) => {
    return state.tempMerchantObj
  }
}
const actions = {
}
const mutations = {
  setNewCreateSubmit (state, val) {
    state.newCreateSubmitted = val
  },
  setSuccessImage (state, val) {
    state.onSuccessImage = val
  },
  setTempMerchantObj (state, obj) {
    state.tempMerchantObj = obj
  },
  setBulkDeleteImage (state, arr) {
    state.bulkDeleteImage.push(arr)
  },
  resetBulkDeleteImage (state) {
    state.bulkDeleteImage = []
  },
  resetBulkUploadImage (state) {
    state.bulkUploadImage = 0
  },
  resetState: (state) => {
    state.newCreateSubmitted = false
    state.onSuccessImage = false
    state.bulkDeleteImage = []
  },
  incrementBulkUpload: (state) => {
    state.bulkUploadImage = state.bulkUploadImage + 1
  },
  decrementBulkUpload: (state) => {
    state.bulkUploadImage = state.bulkUploadImage - 1
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
