const state = {
  pageInfo: {
    title: '',
    links: []
  }
}
const getters = {}
const actions = {
}
const mutations = {
  updateInfo (state, obj) {
    state.pageInfo = obj
  }
}
const computed = {}

export default {
  state,
  getters,
  actions,
  mutations,
  computed
}
