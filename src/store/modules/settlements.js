const state = {
  newCreateSubmitted: false
}
const getters = {
  getCreateSubmitted: (state) => {
    return state.newCreateSubmitted
  }
}
const actions = {
}
const mutations = {
  setCreateSubmit (state, val) {
    state.newCreateSubmitted = val
  },
  resetState: () => {
    state.newCreateSubmitted = false
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
