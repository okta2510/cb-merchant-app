const state = {
  auth: {}
}
const getters = {
  getAuth: (state) => {
    return state.auth
  }
}
const actions = {
}
const mutations = {
  setAuth (state, val) {
    state.auth = val
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
