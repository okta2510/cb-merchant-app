let reload = ''

const state = {
  auth: {}
}
const getters = {}
const actions = {
  increment (context) {
    context.commit('increment')
  }
}
const mutations = {
  notifSet (state, obj) {
    let self = this
    clearTimeout(reload)
    state.auth = obj
    reload = setTimeout(function () {
      self.commit('notifEmpty')
    }, 5000)
  },
  notifEmpty (state) {
    state.auth = {}
  }
}
const computed = {}

export default {
  state,
  getters,
  actions,
  mutations,
  computed
}
