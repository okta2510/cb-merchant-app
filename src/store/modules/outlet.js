const state = {
  listPhotoMenu: [],
  listPhotoFood: [],
  listPhotoAmbience: [],
  listUpdatedPhotos: [],
  listSelectedPhotos: []
}
const getters = {
  getPhotoMenu: (state) => {
    return state.listPhotoMenu
  },
  getPhotoFood: (state) => {
    return state.listPhotoFood
  },
  getPhotoAmbience: (state) => {
    return state.listPhotoAmbience
  },
  getPhotoUpdated: (state) => {
    return state.listUpdatedPhotos
  },
  getPhotoSelected: (state) => {
    return state.listSelectedPhotos
  }
}
const mutations = {
  setPhotoMenu: (state, obj) => {
    let arr = obj.file
    arr.sort((a, b) => a.idxOrder - b.idxOrder)
    state.listPhotoMenu = arr
  },
  setPhotoFood: (state, obj) => {
    let arr = obj.file
    arr.sort((a, b) => a.idxOrder - b.idxOrder)
    state.listPhotoFood = arr
  },
  setPhotoAmbience: (state, obj) => {
    let arr = obj.file
    arr.sort((a, b) => a.idxOrder - b.idxOrder)
    state.listPhotoAmbience = arr
  },
  setPhotoUpdated: (stadte, arr) => {
    state.listUpdatedPhotos = arr
  },
  setPhotoSelected: (state, obj) => {
    obj.action = 'delete'
    state.listSelectedPhotos.push(obj)
  },
  setPrimary: (state, obj) => {
    let arr = []
    state[obj.state].forEach(element => {
      if (element.id === obj.id) {
        element.isBannerPrimary = true
      } else {
        element.isBannerPrimary = false
      }
      arr.push(element)
    })
    state[obj.state] = arr
  },
  removePhoto: (state, obj) => {
    let temp = state.listSelectedPhotos
    temp = temp.filter(el => el.id !== obj.id)
    state.listSelectedPhotos = temp
  },
  resetBulk: (state) => {
    state.listUpdatedPhotos = []
    state.listSelectedPhotos = []
  },
  resetListPhoto: (state) => {
    state.listPhotoMenu = []
    state.listPhotoFood = []
    state.listPhotoAmbience = []
  }
}
const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
