const state = {
  newCreateSubmitted: false,
  tempEdit: {}
}
const getters = {
  getCreateSubmitted: (state) => {
    return state.newCreateSubmitted
  },
  getTempEdit: (state) => {
    return state.tempEdit
  }
}
const actions = {
}
const mutations = {
  setCreateSubmit (state, val) {
    state.newCreateSubmitted = val
  },
  setTempEdit (state, obj) {
    state.tempEdit = obj
  },
  resetState: () => {
    state.newCreateSubmitted = false
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
