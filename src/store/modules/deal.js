const state = {
  newDeal: {}
}
const getters = {}
const actions = {
}
const mutations = {
  updateDeal (state, obj) {
    state.newDeal = obj
  }
}
const computed = {}

export default {
  state,
  getters,
  actions,
  mutations,
  computed
}
