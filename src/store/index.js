import Vue from 'vue'
import Vuex from 'vuex'
import VueLocalStorage from 'vue-localstorage'
import notification from './modules/notification'
import profile from './modules/profile'
import deal from './modules/deal'
import merchants from './modules/merchants'
import settlements from './modules/settlements'
import breadCrumb from './modules/breadCrumb'
import paymentInterface from './modules/paymentInterface'
import outlet from './modules/outlet'

Vue.use(Vuex)
Vue.use(VueLocalStorage)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    notification, profile, deal, merchants, settlements, breadCrumb, paymentInterface, outlet
  },
  strict: debug
})
