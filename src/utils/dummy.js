export const rating = {
  statusRating: [
    { name: 'New', value: 'NEW' },
    // { name: 'Reported', value: 'REPORTED' },
    { name: 'Approved', value: 'APPROVED' },
    { name: 'Hidden', value: 'UNPUBLISHED' }
  ]
}

export const statusCampaign = [
  {
    title: 'ACTIVE',
    value: 'ACTIVE'
  },
  {
    title: 'COMPLETED',
    value: 'COMPLETED'
  },
  {
    title: 'PENDING',
    value: 'PENDING'
  }
]

export const campaignAssetsListType = [
  {
    title: 'HOME_BANK_PROMO',
    value: 'HOME_BANK_PROMO'
  },
  {
    title: 'BANNER_POPULAR_REWARD',
    value: 'BANNER_POPULAR_REWARD'
  },
  {
    title: 'HOME_ONGOING_PROMO',
    value: 'HOME_ONGOING_PROMO'
  },
  {
    title: 'OTHER_PROMO',
    value: 'OTHER_PROMO'
  },
  {
    title: 'BANNER_SPOTLIGHT',
    value: 'BANNER_SPOTLIGHT'
  },
  {
    title: 'BANNER_LINKED_CARD',
    value: 'BANNER_LINKED_CARD'
  },
  {
    title: 'BANNER_ACQUISITION',
    value: 'BANNER_ACQUISITION'
  }
]

export const campaignAssetsListStatus = [
  {
    title: 'ACTIVE',
    value: 'ACTIVE'
  },
  {
    title: 'INACTIVE',
    value: 'INACTIVE'
  }
]

export const deal = {
  statusCreate: [
    { name: 'PENDING', id: 1 }
  ],
  statusEdit: [
    { name: 'PENDING', id: 1 },
    { name: 'PUBLISHED', id: 2 },
    { name: 'COMPLETED', id: 3 },
    { name: 'CANCELLED', id: 4 }
  ],
  statusfilterDealsType: [
    { name: 'ALL' },
    { name: 'ONLINE' },
    { name: 'OFFLINE' }
  ],
  statusDealsType: [
    { name: 'ONLINE', temp: 'ONLINE' },
    { name: 'OFFLINE', temp: 'OFFLINE' }
  ],
  statusEditPending: [
    { name: 'PUBLISHED', id: 2 },
    { name: 'CANCELLED', id: 4 }
  ],
  statusEditPublish: [
    { name: 'COMPLETED', id: 3 }
  ],
  statusFilter: [
    { name: 'All', id: 0 },
    { name: 'Pending', id: 1 },
    { name: 'Published', id: 2 },
    { name: 'Completed', id: 3 },
    { name: 'Cancelled', id: 4 }
  ],
  outlet_name: ['jakarta pusat', 'jakarta barat', 'jakarta timur', 'jakarta selatan', 'jakarta utara'],
  merchant_name: ['Mc. Donalds', 'KFC', 'Carls Burger', 'Ayam Sabana'],
  success_fee_percent: [ 10, 20, 30, 40, 50, 60, 70 ]
}

export const dealDetail = {
  test: 'ini word test',
  events: '',
  newDeal: {
    name: 'Mcdonalds happy terus',
    banner: {
      path: '',
      name: ''
    },
    desc_eng: 'This is Description of McDonalds Happy Terus Outlet Sarinah. This is Description of McDonald’s Happy Terus Outlet Sarinah. This is Description of McDonalds Happy Terus Outlet Sarinah. This is Description of McDonalds Happy Terus Outlet Sarinah. ',
    desc_ind: 'This is Description of McDonalds Happy Terus Outlet Sarinah. This is Description of McDonalds Happy Terus Outlet Sarinah. This is Description of McDonald’s Happy Terus Outlet Sarinah. This is Description of McDonalds Happy Terus Outlet Sarinah. ',
    terms_eng: '',
    terms_ind: '',
    publish_date_start: '2019-02-01',
    publish_date_end: '2019-07-31',
    validity_date_start: '2019-02-01',
    validity_date_end: '2019-07-31',
    deal_status: 'cancelled',
    deal_redeem: 2,
    deal_quantity: 200,
    outlet_address: 'Jalan M.H. Thamrin 11 - Gedung Sarinah Ground Floor',
    outlet_province: 'DKI Jakarta',
    outlet_city: 'Jakarta Pusat',
    outlet_latitude: '64,6796232684',
    outlet_longitude: '103,7575859',
    normal_price: 'Rp100.000',
    selling_price: 'Rp50.000',
    percentage_price: '50%',
    purchase_price: 'Rp20.000',
    success_fee: 'Rp2.500',
    cashback_user: '10%',
    merchant_name: '',
    outlet_name: 'McDonalds Sarinah Thamrin',
    success_fee_percent: ''
  }
}

export const dealList = [
  {
    id: 219,
    deal_name: 'BK Gebyar Promo',
    outlet_name: 'BK Grand Indonesia',
    status: 'PUBLISHED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 105,
    dealsName: 'McD Gratis 2 get 4',
    outletName: 'McD Sarinah',
    status: 'CANCELLED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 104,
    dealsName: 'Shabu Hachi ',
    outletName: 'Shabu Hachi Kemang',
    status: 'COMPLETED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 118,
    dealsName: 'BK Gebyar Promo',
    outletName: 'BK Grand Indonesia',
    status: 'PUBLISHED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 105,
    dealsName: 'McD Gratis 2 get 4',
    outletName: 'McD Sarinah',
    status: 'CANCELLED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 104,
    dealsName: 'Shabu Hachi ',
    outletName: 'Shabu Hachi Kemang',
    status: 'COMPLETED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 118,
    dealsName: 'BK Gebyar Promo',
    outletName: 'BK Grand Indonesia',
    status: 'PUBLISHED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 105,
    dealsName: 'McD Gratis 2 get 4',
    outletName: 'McD Sarinah',
    status: 'CANCELLED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 104,
    dealsName: 'Shabu Hachi ',
    outletName: 'Shabu Hachi Kemang',
    status: 'COMPLETED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 218,
    deal_name: 'McD Gratis 2 get 4',
    outlet_name: 'McD Sarinah',
    status: 'CANCELLED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  },
  {
    id: 104,
    dealsName: 'Shabu Hachi ',
    outletName: 'Shabu Hachi Kemang',
    status: 'COMPLETED',
    publishStartDate: '1563296400000',
    publishEndDate: '1561050000000',
    validityStartDate: '1563296400000',
    validityEndDate: '1561050000000'
  }
]

export const merchantList = {
  'content': [
    {
      business_type: 'FOOD',
      city: 'JAKARTA',
      create_ts: 1568625801147,
      merchant_pubid: '209a7461-a9c3-4c98-9e38-32b557504fa8',
      status: 'INITIATED',
      trade_name: '1QATESTING'
    },
    {
      business_type: 'Food and Beverages',
      city: 'JAKARTA PUSAT',
      create_ts: 1568360656534,
      merchant_pubid: 'e88adc3e-4c6a-48a6-b921-21488c60b167',
      status: 'INITIATED',
      trade_name: 'test bank name'
    },
    {
      business_type: 'LIFESTYLE',
      city: 'JAKARTA BARAT',
      create_ts: 1568085688644,
      merchant_pubid: '60c235f9-1ce7-4e2d-864d-ebf68a172041',
      status: 'ACTIVE',
      trade_name: 'daebak inc'
    }
  ],
  code: 200,
  totalPages: 59,
  totalRows: 583,
  pageNum: 1
}

export const merchant = {
  select: [
    {
      name: 'select 01',
      id: 1
    },
    {
      name: 'select 02',
      id: 2
    },
    {
      name: 'select 03',
      id: 3
    },
    {
      name: 'select 05',
      id: 4
    },
    {
      name: 'select 06',
      id: 5
    }
  ],
  status: [
    { name: 'active' },
    { name: 'initiated' },
    { name: 'inactive' },
    { name: 'suspended' },
    { name: 'deactivated' }
  ],
  parent_merchant: [
    { name: 'merchant 01' },
    { name: 'merchant 02' },
    { name: 'merchant 03' }
  ],
  tags: [
    {
      id: 1,
      tags: 'Featured'
    },
    {
      id: 2,
      tags: 'Fast Food'
    },
    {
      id: 3,
      tags: 'Casual Dining'
    },
    {
      id: 4,
      tags: 'Coffee Shop'
    },
    {
      id: 5,
      tags: 'Fine Dining'
    },
    {
      id: 6,
      tags: 'Group Dining'
    },
    {
      id: 7,
      tags: 'Movies'
    },
    {
      id: 8,
      tags: 'Beauty'
    },
    {
      id: 9,
      tags: 'Meals'
    },
    {
      id: 10,
      tags: 'Event'
    },
    {
      id: 11,
      tags: 'Shop'
    }
  ],
  business_type: [
    { name: 'type01' },
    { name: 'type02' },
    { name: 'type03' },
    { name: 'type04' },
    { name: 'type05' }
  ]
}
// ###
export const merchantListStatus = [
  { name: 'INITIATED' },
  { name: 'ACTIVE' },
  { name: 'TERMINATED' }
]
// ###
export const OutletStatus = [
  { name: 'INITIATED', value: 'INITIATED' },
  { name: 'ACTIVE', value: 'ACTIVE' },
  { name: 'INACTIVE', value: 'INACTIVE' },
  { name: 'SUSPENDED', value: 'SUSPENDED' },
  { name: 'TEMPORARILY CLOSED', value: 'TEMPORARILY CLOSED' },
  { name: 'DEACTIVATED', value: 'DEACTIVATED' }
]
// ###
export const PaymentTransaction = [
  { name: 'online', value: true },
  { name: 'offline', value: false }
]
// ###
export const merchantListBusinessType = [
  {
    name: 'Luxury Dining',
    value: 'FOOD_LUX'
  },
  {
    name: 'Food and Beverages',
    value: 'FOOD'
  },
  {
    name: 'Health and Beauty',
    value: 'BEAUTY'
  },
  {
    name: 'Karaoke',
    value: 'ENTERTAINMENT'
  },
  {
    name: 'Fashion',
    value: 'LIFESTYLE'
  },
  {
    name: 'Grocery',
    value: 'GROCERY'
  },
  {
    name: 'Shop',
    value: 'SHOP'
  },
  {
    name: 'Event',
    value: 'EVENT'
  }
]

export const listOutletOptions = [
  { name: 'Cashbac Pay', value: 'CASHBAC_PAY' },
  { name: 'Cashbac Deals', value: 'CASHBAC_DEALS' },
  { name: 'Cashbac Earn', value: 'CASHBAC_EARN' },
  { name: 'Cashbac Easy Pay', value: 'CASHBAC_EASY_PAY' },
  { name: 'Cashbac WA Delivery', value: 'CASHBAC_DELIVERY_WA' },
  { name: 'Cashbac Installment', value: 'CASHBAC_INSTALLMENT' }
]

export const listDeliveryInstructions = [
  { name: 'Easy Pay', value: 'DELIV-EASYPAY' },
  { name: 'Show Barcode', value: 'DELIV-SHOW-BARCODE' },
  { name: 'Scan Barcode', value: 'DELIV-SCAN-BARCODE' }
]

export const listRedemptionUnit = [
  { title: 'Day', value: 'DAY' },
  { title: 'Week', value: 'WEEK' },
  { title: 'Month', value: 'MONTH' },
  { title: 'Lifetime', value: 'LIFETIME' }
]
export const listRedemptionGroup = [
  { title: 'Merchant', value: 'MERCHANT' },
  { title: 'Outlet', value: 'OUTLET' },
  { title: 'None', value: 'NONE' }
]
export const listCashbacUnit = [
  { title: 'Plus', value: '+' },
  { title: 'Multiple', value: '*' },
  { title: 'Percentage', value: '%' }
]
export const listCashbacSource = [
  { title: 'Amount', value: 'AMOUNT' },
  { title: 'Paycard', value: 'PAYCARDAMOUNT' },
  { title: 'Cashbac Amount', value: 'CASHBACKAMOUNT' },
  { title: 'Balance', value: '0' }
]
