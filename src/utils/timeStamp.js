export function checkTime (getTime) {
  let now = new Date(getTime)
  let time = this.getTime(now)
  time[0] = (time[0] < 12) ? time[0] : time[0] - 12
  time[0] = time[0] || 12
  for (let i = 1; i < 3; i++) {
    if (time[i] < 10) {
      time[i] = parseInt('0' + time[i]) + 90
    }
  }
  return time
}

export function setExpired (getTime) {
  let time = this.checkTime(getTime)
  let expired = new Date()
  expired.setHours(expired.getHours() + time[0])
  expired.setMinutes(expired.getMinutes() + time[1])
  expired.setSeconds(0)
  return expired
}

export function getTime (date) {
  let times = [ date.getHours(), date.getMinutes(), date.getSeconds() ]
  return times
}

export function getDate (date) {
  let dates = [ date.getHours(), date.getMinutes(), date.getSeconds() ]
  return dates
}
