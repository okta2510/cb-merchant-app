export function auth () {
  return localStorage.getItem('auth_info')
}
export function getAuth () {
  return JSON.parse(this.auth())
}
export function header () {
  let isToken = JSON.parse(this.auth())
  return {
    'token': isToken.token,
    'user': 'okta',
    'email': 'okta@yahoo.com',
    'page': 'getdeal',
    'menu': 'menu',
    'action': 'action',
    'message': 'message'
  }
}
