import * as dummy from '@/utils/dummy'
export function getMerchant (obj) {
  let { merchant, fee, account } = obj
  let payload = {
    newMerchant: {
      merchant: {
        merchantOptions: merchant.merchantOptions || [],
        useNewCashbackScheme: merchant.useNewCashbackScheme,
        tradeName: merchant.tradeName,
        businessType: merchant && merchant.businessType ? filteringArr(merchant.businessType, dummy.merchantListBusinessType).value : [],
        listSelectedTags: parsingTags([...merchant.tags]),
        tags: [...merchant.tags],
        hqMdn: merchant.hqMdn,
        status: merchant.status,
        statusObj: {
          name: merchant.status
        },
        hqProvince: merchant.hqProvince,
        hqProvinceObj: {
          name: merchant.hqProvince
        },
        hqCity: merchant.hqCity,
        hqCityObj: {
          name: merchant.hqCity
        },
        hqZipCode: merchant.hqZipCode,
        hqAddress: merchant.hqAddress,
        parentPubId: merchant.parentPubId,
        parent: {
          merchant_pubid: merchant.parentPubId,
          trade_name: merchant.tradeName
        },
        businessTypeOptions: {
          name: merchant.businessType,
          value: merchant && merchant.businessType ? filteringArr(merchant.businessType, dummy.merchantListBusinessType).value : []
        },
        cashbacPercent: merchant.cashbacPercent, // cashback percentage json
        ribbonText: merchant.ribbonText,
        createBy: '',
        description: '',
        website: merchant.website || '',
        appsName: merchant.appsName || ''
      },
      account: {
        npwp: account.npwp,
        npwpAddress: account.npwpAddress,
        merchantPubId: account.merchantPubId,
        bankName: account.bankName,
        bankBranch: account.bankBranch,
        email: account.email,
        legalName: account.legalName,
        accountName: account.accountName,
        accountNumber: account.accountNumber,
        receiveReceipt: account.receiveReceipt,
        remarkBankTransfer: account.remarkBankTransfer
      },
      fee: {
        pubId: fee.pubId,
        merchantPubId: fee.merchantPubId,
        newCustomerFee: parseFloat(fee.newCustomerFee), // new customer affiliate fee
        retainCustomerFee: parseFloat(fee.retainCustomerFee), // returning customer
        lapseCustomerFee: parseFloat(fee.lapseCustomerFee), // lased customer
        comissionNewCustomerFee: fee.comissionNewCustomerFee,
        comissionRetainCustomerFee: fee.comissionRetainCustomerFee,
        comissionLapseCustomerFee: fee.comissionLapseCustomerFee,
        cashbacNewCustomerFee: fee.cashbacNewCustomerFee,
        cashbacRetainCustomerFee: fee.cashbacRetainCustomerFee,
        cashbacLapseCustomerFee: fee.cashbacLapseCustomerFee,
        lapseCustomerMaxCashback: fee.lapseCustomerMaxCashback,
        lapseCustomerMaxAffiliate: fee.lapseCustomerMaxAffiliate,
        newCustomerMaxCashback: fee.newCustomerMaxCashback, // cap new customer cashback
        newCustomerMaxAffiliate: fee.newCustomerMaxAffiliate, // cap affiliate new customer
        retainCustomerMaxCashback: fee.retainCustomerMaxCashback,
        retainCustomerMaxAffiliate: fee.retainCustomerMaxAffiliate,
        status: 'ACTIVE',
        terms: [fee.terms ? fee.terms[0] : ''], // terms
        description: fee.description
      },
      cashback: {
        newCustomerFee: 0,
        retainCustomerFee: 0,
        lapseCustomerFee: 0,
        comissionNewCustomerFee: 0,
        comissionRetainCustomerFee: 0,
        comissionLapseCustomerFee: 0
      }
    }
  }

  return payload
}

export function getCashbacPercent (response) {
  if (response) {
    let obj = JSON.parse(response)
    let percent = [{
      style: '',
      text: '',
      options: { style: '' }
    }, {
      style: '',
      text: '',
      options: { style: '' }
    }, {
      style: '',
      text: '',
      options: { style: '' }
    }]
    // ###
    if (obj) {
      obj.forEach((element, index) => {
        percent[index] = {
          style: element.style,
          text: element.text,
          options: {
            style: element.style
          }
        }
      })
    }
    return percent
  } else {
    return [{
      style: '',
      text: '',
      options: { style: '' }
    }, {
      style: '',
      text: '',
      options: { style: '' }
    }, {
      style: '',
      text: '',
      options: { style: '' }
    }]
  }
}
export function getRibbonText (response) {
  let options = [{ value: 'additional', text: 'Additional Promo' }, { value: 'normal', text: 'Normal Promo' }]
  if (response) {
    let obj = JSON.parse(response)
    let ribbon = {
      type: obj.type,
      options: {
        value: obj.type,
        text: ''
      },
      title: [{
        style: '',
        text: '',
        options: {
          style: ''
        }
      }, {
        style: '',
        text: '',
        options: {
          style: ''
        }
      }, {
        style: '',
        text: '',
        options: {
          style: ''
        }
      }]
    }
    // ###
    if (obj.title) {
      obj.title.forEach((element, index) => {
        ribbon.title[index] = {
          style: element.style || '',
          text: element.text || '',
          options: {
            style: element.style || ''
          }
        }
      })
      ribbon.type = obj.type
      ribbon.options = {
        value: obj.type,
        text: options.filter(name => name.value === obj.type)[0].text
      }
    }
    return ribbon
  } else {
    return {
      title: [{
        style: '',
        text: '',
        options: {
          style: ''
        }
      }, {
        style: '',
        text: '',
        options: {
          style: ''
        }
      }, {
        style: '',
        text: '',
        options: {
          style: ''
        }
      }],
      type: '',
      options: {
        text: '',
        value: ''
      }
    }
  }
}

export function getLogo (arr) {
  let result = arr.filter(data => data.type === 'LOGO')
  let total = result.length - 1
  if (result.length > 0) {
    return {
      length: result.length,
      type: 'DEFAULT_LOGO',
      id: result[total].id,
      blob: result[total].media_url,
      name: trimNameImage(result[total].media_url),
      action: 'UPDATE'
    }
  } else {
    return {}
  }
}

export function getMainBanner (arr) {
  let result = arr.filter(data => data.type === 'MAIN_BANNER')
  if (result.length > 0) {
    return {
      type: 'DEFAULT_MAIN_BANNER',
      id: result[0].id,
      blob: result[0].media_url,
      name: trimNameImage(result[0].media_url),
      action: 'UPDATE'
    }
  } else {
    return {}
  }
}

export function getBanners (arr) {
  let result = arr.filter(data => data.type === 'BANNER')
  if (result.length > 0) {
    let newResult = []
    result.forEach((element, index) => {
      newResult.push({
        type: 'DEFAULT_BANNER',
        id: element.id,
        blob: element.media_url,
        fileName: trimNameImage(element.media_url),
        action: 'UPDATE'
      })
    })
    return newResult
  } else {
    return []
  }
}

let parsingTags = function (arr) {
  let newArr = []
  arr.forEach((element, index) => {
    newArr.push({
      name: element
    })
  })
  return newArr
}

let trimNameImage = function (str) {
  let name = str.split('/')
  return (name[name.length - 1])
}

let filteringArr = function (val, arr) {
  let filtered = arr.filter(unit => unit.name === val)
  return filtered[0]
}
