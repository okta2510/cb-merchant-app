export function selectList (arr, name, detail) {
  let result = []
  if (arr) {
    for (let i = 0; i < arr.length; i++) {
      let obj = {
        name: arr[i][name],
        id: arr[i]['pubId'],
        detail: arr[i]
      }
      result.push(obj)
    }
    return result
  } else {
    return result
  }
}

export function createDeal (obj) {
  let postData = {
    bannerBlob: obj.banner.blob,
    dealsBatch: {
      createBy: obj.userName,
      dealsDescriptions: obj.desc_ind,
      dealsDescriptionsEn: '',
      dealsName: obj.name,
      dealsPrefix: obj.prefix,
      dealsPrice: parseInt(obj.selling_price),
      dealsTerms: obj.terms_ind,
      dealsTermsEn: '',
      dealsTitle: obj.name,
      tags: obj.tags,
      dealsType: obj.deal_type.name === 'OFFLINE' ? 'PRODUCT' : 'PRODUCT_ONLINE',
      maxRedeemDaily: parseInt(obj.deal_redem),
      outletPubId: obj.outlet_name,
      productPrice: parseInt(obj.purchase_price),
      publishEndTs: obj.publish_date_end,
      publishStartTs: obj.publish_date_start,
      effectivePeriod: parseInt(obj.effective_period),
      quantity: parseInt(obj.deal_quantity),
      sellingPrice: parseInt(obj.normal_price),
      status: obj.deal_status.name.toUpperCase(),
      validityEndTs: obj.validity_date_end,
      validityStartTs: obj.validity_date_start,
      redemptionGuide: obj.redemptionGuide,
      redemptionGuideEn: '',
      importantNote: obj.importantNote,
      importantNoteEn: '',
      isNeedRedemption: obj.isNeedRedemption
    },
    dealsFee: {
      cashbackToUser: parseFloat(obj.cashback_user),
      createBy: obj.userName,
      successFee: parseFloat(obj.success_fee)
    },
    fileName: 'banner',
    fileType: obj.banner.fileType,
    thumbnail: {
      blob: obj.thumbnail.blob,
      fileName: 'thumb_',
      fileType: obj.thumbnail.fileType
    }
  }

  return postData
}

export function editDeal (obj) {
  let postData = {
    bannerBlob: obj.banner.blob,
    thumbnailBlob: obj.thumbnail.blob,
    dealsBatch: {
      createBy: obj.createBy_deal_batch,
      dealsDescriptions: obj.desc_ind,
      dealsDescriptionsEn: '',
      dealsName: obj.name,
      dealsPrefix: obj.prefix,
      tags: obj.tags,
      dealsPrice: parseInt(obj.selling_price),
      dealsTerms: obj.terms_ind,
      dealsTermsEn: '',
      dealsTitle: obj.name,
      dealsType: obj.deal_type.name === 'OFFLINE' ? 'PRODUCT' : 'PRODUCT_ONLINE',
      earnExtraCashback: false,
      id: obj.id,
      maxRedeemDaily: parseInt(obj.deal_redem),
      outletPubId: obj.outlet_name[0],
      productPrice: parseInt(obj.purchase_price),
      publishEndTs: obj.publish_date_end,
      publishStartTs: obj.publish_date_start,
      effectivePeriod: parseInt(obj.effective_period),
      quantity: parseInt(obj.deal_quantity),
      sellingPrice: parseInt(obj.normal_price),
      status: obj.deal_status.name.toUpperCase(),
      validityEndTs: obj.validity_date_end,
      redemptionGuide: obj.redemptionGuide,
      importantNote: obj.importantNote,
      validityStartTs: obj.validity_date_start,
      dealsImageUrl: obj.banner.blob,
      thumbnailUrl: obj.thumbnail.blob,
      isNeedRedemption: obj.isNeedRedemption
    },
    dealsFee: {
      cashbackToUser: parseFloat(obj.cashback_user),
      createBy: obj.createBy_dealsFee,
      // createTs: 0,
      dealsBatchId: obj.id,
      id: obj.id_dealsFee,
      successFee: parseFloat(obj.success_fee),
      updateBy: obj.createBy_dealsFee
      // updateTs: 0
    },
    thumbnail: {
      blob: obj.thumbnail.blob,
      fileName: 'thumb',
      fileType: obj.thumbnail.fileType
    },
    fileName: 'banner',
    fileType: obj.banner.fileType
  }

  return postData
}

export function getDeal (data) {
  let obj = {}
  let key = Object.keys(data)

  for (let i = 0; i < key.length; i++) {
    if (key[i] === 'dealsBatch') {
      obj.id_deal_batch = data[key[i]].id
      obj.status_deal_type = data[key[i]].dealsType === 'PRODUCT' ? 'OFFLINE' : 'ONLINE'
      obj.status_deal_batch = data[key[i]].status
      obj.createBy_deal_batch = data[key[i]].createBy
      obj.deal_tags = data[key[i]].tags
    } else if (key[i] === 'outlet') {
      obj.id_outlet = data[key[i]].id
      obj.status_outlet = data[key[i]].status
    } else if (key[i] === 'dealsFee') {
      obj.id_dealsFee = data[key[i]].id
      obj.createBy_dealsFee = data[key[i]].createBy
    } else if (key[i] === 'merchant') {
      obj.status_outlet = data[key[i]].status
    }
    Object.assign(obj, data[key[i]])
  }
  let getData = {
    id: obj.id_deal_batch,
    name: obj.dealsName,
    prefix: obj.dealsPrefix,
    tags: obj.deal_tags,
    thumbnail: {
      blob: obj.thumbnailUrl,
      fileName: 'thumb',
      filetype: ''
    },
    banner: {
      blob: obj.dealsImageUrl,
      fileName: 'banner',
      filetype: ''
    },
    desc_ind: obj.dealsDescriptions,
    terms_ind: obj.dealsTerms,
    publish_date_start: obj.publishStartTs,
    publish_date_end: obj.publishEndTs,
    validity_date_start: obj.validityStartTs,
    validity_date_end: obj.validityEndTs,
    effective_period: obj.effectivePeriod,
    deal_type: {
      name: obj.status_deal_type,
      value: ''
    },
    deal_status: {
      name: obj.status_deal_batch,
      value: ''
    },
    createBy_deal_batch: obj.createBy_deal_batch,
    createBy_dealsFee: obj.createBy_dealsFee,
    id_dealsFee: obj.id_dealsFee,
    deal_redem: obj.maxRedeemDaily,
    deal_quantity: obj.quantity,
    outlet_address: obj.address,
    outlet_province: obj.province,
    outlet_city: obj.city,
    outlet_latitude: obj.latitude,
    outlet_longitude: obj.longitude,
    normal_price: obj.sellingPrice,
    selling_price: obj.dealsPrice,
    purchase_price: obj.productPrice,
    success_fee: obj.successFee,
    cashback_user: obj.cashbackToUser,
    redemptionGuide: obj.redemptionGuide,
    importantNote: obj.importantNote,
    isNeedRedemption: obj.isNeedRedemption,
    merchant_name: {
      detail: '',
      id: obj.merchantPubId,
      name: obj.tradeName
    },
    outlet_name: [obj.outletPubId],
    outletName: obj.outletName,
    get_outlet: [{
      detail: '',
      id: obj.outletPubId,
      name: obj.outletName
    }],
    merchant_list: [],
    outlet_list: []
  }

  return getData
}
