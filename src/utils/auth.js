
import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
Vue.use(VueLocalStorage)

export function expired () {
  let auth = localStorage.getItem('auth_info')
  auth = JSON.parse(auth)
  if (auth) {
    let now = new Date()
    auth.expires_time = new Date(auth.expires_time)

    if (auth.expires_time < now) {
      return true
    } else {
      return false
    }
  }
}
