import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
//
import 'bootstrap'
// import 'bootstrap/dist/css/bootstrap.min.css'
//
import VueResource from 'vue-resource'
//
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faUser, faPlus, faTachometerAlt, faCube, faPercent, faBullhorn, faChartLine, faUsers, faUserSecret, faImage, faHandshake, faCreditCard, faFolder, faArchive, faTrash, faPen } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//
import VueLocalStorage from 'vue-localstorage'
import * as auth from './utils/auth'
//
import Vuex from 'vuex'
//
import 'es6-promise/auto'
//
import VueFlashMessage from 'vue-flash-message'
require('vue-flash-message/dist/vue-flash-message.min.css')

Vue.use(VueFlashMessage, {
  messageOptions: {
    autoEmit: true,
    pauseOnInteract: true
  }
})
Vue.use(Vuex)
Vue.use(VueLocalStorage)
Vue.use(VueResource)
Vue.config.productionTip = false
Vue.config.performance = true
window.$ = window.jQuery = require('jquery')

// Vue.http.options.root = 'http://172.32.2.108:9100'
// Vue.http.headers.common['Authorization'] = 'Basic Y2FzaGJhYzpzZWNyZXQ='
// Vue.http.headers.common['Access-Control-Request-Method'] = '*'
// Vue.http.headers.common['Access-Control-Allow-Origin'] = 'http://172.32.2.108'
// Vue.http.headers.common['useCredentails'] = '*'
Vue.http.options.credentials = true
Vue.http.options.emulateHTTP = true
Vue.http.interceptors.push(function (request, next) {
  let self = this
  // put your iterceptor function call here
  if (auth.expired()) {
    this.$router.push({
      path: '/auth/logout'
    }, () => {
      self.flash(`Authentication Expired or not valid`, 'error', {
        timeout: 5000
      })
    })
  }
})

library.add(faCoffee, faUser, faPlus, faTachometerAlt, faCube, faPercent, faBullhorn, faChartLine, faUsers, faUserSecret, faImage, faHandshake, faCreditCard, faFolder, faArchive, faTrash, faPen)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
