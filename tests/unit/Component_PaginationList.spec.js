import { shallowMount, createLocalVue } from '@vue/test-utils'
import PaginationList from '@/components/PaginationList.vue'
import Vuex from 'vuex'
import BeaconList from '@/components/paymentInterface/beaconsList.vue'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
const $router = new VueRouter()
localVue.use(VueRouter)
localVue.use(BeaconList)
localVue.use(Vuex)

shallowMount(PaginationList, {
  localVue,
  $router
})
const $route = {
  path: '/business-setup/outlet/1/payment-interface',
  query: {
    size: 10,
    pagination: 1,
    sort: 1,
    order: 'desc'
  }
}

it('Check pagination print exact value', async () => {
  const wrapper = shallowMount(PaginationList, {
    mocks: {
      $route,
      $router
    },
    propsData: {
      pageSize: 10,
      results: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      currentPage: 1,
      totalPage: 10,
      totalRows: 100
    }
  })
  const text = wrapper.find('li.page-item.active')
  expect(text.text()).toContain('1')
  expect(wrapper.contains('a.page-link.next-button')).toBe(true)
  expect(wrapper.contains('a.page-link.prev-button')).toBe(false)
})

it('Check pagination prev button', async () => {
  const wrapper = shallowMount(PaginationList, {
    mocks: {
      $route,
      $router
    },
    propsData: {
      pageSize: 10,
      results: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      currentPage: 2,
      totalPage: 10,
      totalRows: 100
    }
  })
  const text = wrapper.find('li.page-item.active')
  expect(text.text()).toContain('2')
  expect(wrapper.contains('a.page-link.next-button')).toBe(true)
  expect(wrapper.contains('a.page-link.prev-button')).toBe(true)
})

it('Check pagination last page', async () => {
  const wrapper = shallowMount(PaginationList, {
    mocks: {
      $route,
      $router
    },
    propsData: {
      pageSize: 10,
      results: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      currentPage: 10,
      totalPage: 10,
      totalRows: 100
    }
  })
  const text = wrapper.find('li.page-item.active')
  expect(text.text()).toContain('10')
  expect(wrapper.contains('a.page-link.next-button')).toBe(false)
  expect(wrapper.contains('a.page-link.prev-button')).toBe(true)
})
