import { shallowMount, createLocalVue } from '@vue/test-utils'
import NavigationUser from '@/components/NavigationUser'
import Vuex from 'vuex'
import authMixin from '@/mixins/auth'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
const router = new VueRouter()
localVue.use(Vuex)
localVue.use(authMixin)
localVue.use(VueRouter)

// check value from component stores
describe('NavigationUser.vue', () => {
  let actions, store, mutations, getters, state, mockUserInfo
  mockUserInfo = {
    expires_in: '86399',
    expires_time: '2021-06-07T22:49:00.826Z',
    refresh_token: 'cb4930f1-3e69-4d51-b342-9b24cf0085b0',
    role: 'MERCHANT_ADMIN_DMG',
    token: '7de967dc-eacc-44c3-8007-52eadb43f8ad',
    user: 'arindito.bima@cashbac.com'
  }
  mockUserInfo = JSON.stringify(mockUserInfo)
  localStorage.setItem('auth_info', mockUserInfo)

  beforeEach(() => {
    actions = {
    }
    state = {
      auth: {}
    }
    mutations = {
      'profile/setAuth': function (state, val) {
        state.auth = val
      }
    }
    getters = {
      'profile/getAuth': function (state, val) {
        return state.auth
      }
    }
    store = new Vuex.Store({
      actions,
      mutations,
      getters,
      state
    })
  })

  it('User name display', async () => {
    const wrapper = shallowMount(NavigationUser, { store, localVue, router })
    const userName = wrapper.find('.user-info-name')
    expect(userName.text()).toContain('arindito.bima@cashbac.com')
  })
})
