import { shallowMount } from '@vue/test-utils'
import DateFormat from '@/components/DateFormat.vue'

it('Passing props and check print unix', async () => {
  const wrapper = shallowMount(DateFormat, {
    propsData: {
      value: 1622780728,
      format: 'YYYY-MMMM-DD hh:mm:ss',
      isUnix: true
    }
  })

  expect(wrapper.text()).toContain('2021-June-04 11:25:28')
})

it('Passing props and check print non-unix', async () => {
  const wrapper = shallowMount(DateFormat, {
    propsData: {
      value: 1622782016000,
      format: 'YYYY-MMMM-DD',
      isUnix: false
    }
  })

  expect(wrapper.text()).toContain('2021-June-04')
})
