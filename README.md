# cb-webapp-vue

## Before Setup
We are using npm package here, so for easy maintain versioning of npm version 12.3.1, you can use nvm for handle npm version.

## Installing NVM
[installing NVM](https://github.com/nvm-sh/nvm)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Get .env config
[copy files .env from here](https://code.xmaric.com/cashbac/cb-webapp-spa-config)

### We are using modes & environment variable
if you run it on local make sure you have .env.development.local, or example:
```
VUE_APP_BUSINESS=localhost:5000/
VUE_APP_TITLE="Internal Dashboard DEV"
VUE_APP_API="http://172.32.2.108:20400/1.0/"
```


### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### For unit test with jest
```
"babel-jest": "23.4.2"
"babel-core": "7.0.0-bridge.0"

```

### for setting coverage test

https://jestjs.io/docs/configuration#collectcoverage-boolean